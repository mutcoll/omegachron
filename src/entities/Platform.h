/**
 * @file Platform.h
 */

#ifndef OMEGACHRON_PLATFORM_H
#define OMEGACHRON_PLATFORM_H

#include <vector>
#include <randomize/math/Vector3D.h>
#include <graphics/Geometry.h>
#include "TimeBender.h"

class Platform {
public:
    Vector3D center;
    double sizeX, sizeY, sizeZ;
    Vector3D animationDirection;
    double period;
    double instant;
    Vector3D speed;

    TimeRotation timeRotation;

    Platform(const Vector3D &center, double sizeX, double sizeY, double sizeZ, const Vector3D &animationDirection,
            double period, TimeRotation timeRotation) : center(center), sizeX(sizeX), sizeY(sizeY), sizeZ(sizeZ),
            animationDirection(animationDirection), period(period), timeRotation(timeRotation) {
        instant = 0;
        speed = {0, 0, 0};
    }

    std::vector<Quad> getPlanes() const {
        Vector3D vertex{-0.5 * Vector3D{sizeX, sizeY, sizeZ}};
        Vector3D oppositeVertex{ 0.5 * Vector3D{sizeX, sizeY, sizeZ}};
        std::vector<Quad> planes{
                buildPlane(center + vertex, Vector3D{sizeX, 0, 0}, Vector3D{0, 0, sizeZ}),
                buildPlane(center + vertex, Vector3D{0, sizeY, 0}, Vector3D{sizeX, 0, 0}),
                buildPlane(center + vertex, Vector3D{0, 0, sizeZ}, Vector3D{0, sizeY, 0}),
                buildPlane(center + oppositeVertex, Vector3D{-sizeX, 0, 0}, Vector3D{0, -sizeY, 0}),
                buildPlane(center + oppositeVertex, Vector3D{0, -sizeY, 0}, Vector3D{0, 0, -sizeZ}),
                buildPlane(center + oppositeVertex, Vector3D{0, 0, -sizeZ}, Vector3D{-sizeX, 0, 0}),
        };
        return planes;
    }
};

using Platforms = std::vector<Platform>;

#endif //OMEGACHRON_PLATFORM_H
