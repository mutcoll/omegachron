/**
 * @file TimeBender.h
 */

#ifndef OMEGACHRON_TIMEBENDER_H
#define OMEGACHRON_TIMEBENDER_H

#include <vector>
#include <string>
#include "core.h"
#include "randomize/math/Vector3D.h"


enum class TimeRotation {
    TOWARDS_T1,
    TOWARDS_T2,
    AWAY_FROM_T1,
    AWAY_FROM_T2,
    TOWARDS_HALFWAY_T1_AND_T2,
    AWAY_FROM_HALFWAY_T1_AND_T2,
    ROTATE_FROM_T1_TO_T2,
    ROTATE_FROM_T2_TO_T1
};

struct TimeBender {
    Vector3D centerPosition;
    float size;
    float influenceSize;
    Color color;
    TimeRotation timeRotation;
};

inline double approachAngle(double targetAngle, double currentAngle, double speed) {
    auto difference = std::abs(targetAngle - currentAngle);
    if (difference < 0.001) {
        return targetAngle;
    }
    currentAngle -= targetAngle;
    if (currentAngle >= M_PI) {
        currentAngle -= 2 * M_PI;
    } else if (currentAngle <= -M_PI) {
        currentAngle += 2* M_PI;
    }
    if (currentAngle < 0) {
        currentAngle += speed;
    } else {
        currentAngle -= speed;
    }
    return currentAngle + targetAngle;
}

inline void bendTime(TimeRotation timeRotation, double speed, double &t1, double &t2) {
    auto angle = atan2(t2, t1);
    auto adjustedSpeed = speed;
    switch (timeRotation) {
    case TimeRotation::TOWARDS_T1:
        angle = approachAngle(0, angle, adjustedSpeed);
        break;
    case TimeRotation::AWAY_FROM_T1:
        angle = approachAngle(-M_PI, angle, adjustedSpeed);
        break;
    case TimeRotation::TOWARDS_T2:
        angle = approachAngle(M_PI_2, angle, adjustedSpeed);
        break;
    case TimeRotation::AWAY_FROM_T2:
        angle = approachAngle(-M_PI_2, angle, adjustedSpeed);
        break;
    case TimeRotation::TOWARDS_HALFWAY_T1_AND_T2:
        angle = approachAngle(M_PI_4, angle, adjustedSpeed);
        break;
    case TimeRotation::AWAY_FROM_HALFWAY_T1_AND_T2:
        angle = approachAngle(-(M_PI_4 + M_PI_2), angle, adjustedSpeed);
        break;
    case TimeRotation::ROTATE_FROM_T1_TO_T2:
        angle += adjustedSpeed;
        break;
    case TimeRotation::ROTATE_FROM_T2_TO_T1:
        angle -= adjustedSpeed;
        break;
    default:
        throw std::logic_error{"TimeRotation case not handled!: " + std::to_string(static_cast<int>(timeRotation))};
    }
    t1 = cos(angle);
    t2 = sin(angle);
}

using TimeBenders = std::vector<TimeBender>;

#endif //OMEGACHRON_TIMEBENDER_H
