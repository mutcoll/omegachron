/**
 * @file Particle.h
 */

#ifndef PARTICLEAUTOMATON_PARTICLE_H
#define PARTICLEAUTOMATON_PARTICLE_H


#include <vector>
#include <memory>
#include <randomize/math/Vector3D.h>

class Particle {
private:
    Vector3D* const position;
    Vector3D* const speed;
    double* const radius;
    double* const mass;

public:
    Particle(Vector3D *const position, Vector3D *const speed, double *const radius, double *const mass)
            : position(position), speed(speed), radius(radius), mass(mass) {
    }

    const Vector3D &getConstPosition() const {
        return *position;
    }

    const Vector3D &getConstSpeed() const {
        return *speed;
    }

    const double &getConstRadius() const {
        return *radius;
    }

    const double &getConstMass() const {
        return *mass;
    }

    Vector3D &getPosition() {
        return *position;
    }

    Vector3D &getSpeed() {
        return *speed;
    }

    double &getRadius() {
        return *radius;
    }

    double &getMass() {
        return *mass;
    }
};

class BiTimedParticle {
private:
    std::shared_ptr<Vector3D> position;
    std::shared_ptr<Vector3D> speed1;
    std::shared_ptr<Vector3D> speed2;
    std::shared_ptr<double> radius;
    std::shared_ptr<double> mass;
public:
    Particle firstParticleView;
    Particle secondParticleView;

public:
    BiTimedParticle(const Vector3D &_position, const Vector3D &_speed1, const Vector3D &_speed2, double _radius,
            double _mass)
            : position(std::make_shared<Vector3D>(_position)), speed1(std::make_shared<Vector3D>(_speed1)),
            speed2(std::make_shared<Vector3D>(_speed2)),
            radius(std::make_shared<double>(_radius)), mass(std::make_shared<double>(_mass)),
            firstParticleView{position.get(), speed1.get(), radius.get(), mass.get()},
            secondParticleView{position.get(), speed2.get(),radius.get(), mass.get()} {
    }

    const Vector3D &getConstPosition() const {
        return position.operator*();
    }

    const Vector3D &getConstSpeed1() const {
        return speed1.operator*();
    }

    const Vector3D &getConstSpeed2() const {
        return speed2.operator*();
    }

    const double &getConstRadius() const {
        return radius.operator*();
    }

    const double &getConstMass() const {
        return mass.operator*();
    }

    Vector3D &getPosition() const {
        return position.operator*();
    }

    Vector3D &getSpeed1() const {
        return speed1.operator*();
    }

    Vector3D &getSpeed2() const {
        return speed2.operator*();
    }

    double &getRadius() const {
        return radius.operator*();
    }

    double &getMass() const {
        return mass.operator*();
    }

};
using Particles = std::vector<BiTimedParticle>;

#endif //PARTICLEAUTOMATON_PARTICLE_H
