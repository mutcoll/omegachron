/**
 * @file Character.h
 */

#ifndef OMEGACHRON_CHARACTER_H
#define OMEGACHRON_CHARACTER_H

#include <vector>
#include <randomize/math/Vector3D.h>

struct Character {
    double size;
    Vector3D position;
    Vector3D speed;
    Vector3D acceleration;
    bool inAir;
};


#endif //OMEGACHRON_CHARACTER_H
