/**
 * @file Manager.cpp
 */

#include <randomize/utils/exception/StackTracedException.h>
#include "graphics/Geometry.h"
#include "Manager.h"
#include "graphics/drawers.h"
#include "physics/Physics.h"


Manager::Manager(Dispatcher &d, std::shared_ptr<WindowGL3DAnd2D> w)
        : FlyerManagerBase(d, w), character{1, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, false}, physics{true, 15.0},
        window3DAnd2D{w} {
    heightDeviation = Vector3D{0, character.size * 2, 0};
    center = character.position + heightDeviation;
    cameraOffset = Vector3D{0, 1, 7};
    vldr.setPos(center + cameraOffset);
    vldr.setOrientacion(center - vldr.getPos(), Vector3D(0, 1, 0));
    freeCamera = false;
    periodicRotation = false;
    timeBendersPauseTime = false;
    pauseSimulation = false;
    t1Magnitude = cos(M_PI / 4);
    t2Magnitude = sin(M_PI / 4);
    w->setWindowTitle("OmegaChron");

    reloadLevel(predefinedLevels::RAIN_TEST_LEVEL);
}

void Manager::reloadLevel(std::string levelName) {
    levels.clear();
    addPredefinedLevels(levels);
    currentLevelName = levelName;

    character.position = {0, 50, 0};
    character.speed = {0, 0, 0};

    auto &window = tryGetWindow();
    int width, height;
    window.getWindowSize(width, height);
    ui.setupDebugUi(width, height, character, getCurrentLevel());
}

void Manager::resetTime() {
    t1Magnitude = cos(M_PI / 4);
    t2Magnitude = sin(M_PI / 4);
}

inline Level &Manager::getCurrentLevel() {
    return levels.at(currentLevelName);
}

WindowGL3DAnd2D& Manager::tryGetWindow() {
    if (not window3DAnd2D.expired()) {
        return window3DAnd2D.lock().operator*();
    } else {
        throw randomize::utils::exception::StackTracedException{
                "the window was freed while the Manager still wanted to access it"};
    }
}

void Manager::onDisplay(Uint32 ms) {
    auto &window = tryGetWindow();
    int width, height;
    window.getWindowSize(width, height);

    glClearColor(0.1, 0.1, 0.1, 1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPushMatrix();

    if (not freeCamera) {
        vldr.setPos(character.position + heightDeviation + cameraOffset);
        center = character.position + heightDeviation;
    }
    this->FlyerManagerBase::onDisplay( ms);

    if (ui.isDebugging()) {
        drawAxes();
    }
    drawCharacter(character);
    drawLevel(getCurrentLevel());

    glPopMatrix();

    ui.drawUi(width, height, t1Magnitude, t2Magnitude, character, getCurrentLevel());

    window.swapWindow();
}

void Manager::onStep(Uint32 ms) {
    static double totaltime = 0;
    FlyerManagerBase::onStep(ms);
    auto fractions = 5.0;
    auto seconds = ms / 1000.0;
    auto secondsFraction = seconds / fractions;

    if (periodicRotation and int(totaltime) != int(totaltime + seconds) and int(totaltime) % 3 == 0) {
        auto temp = t1Magnitude;
        t1Magnitude = -t2Magnitude;
        t2Magnitude = temp;
    }
    totaltime += seconds;

    for (Uint32 fractionIndex = 0; fractionIndex < fractions; fractionIndex++) {
        bool withinSomeTimeBender = false;
        for (const TimeBender &timeBender : timeBendersAffectingCharacter()) {
            withinSomeTimeBender = true;
            bendTime(timeBender.timeRotation, secondsFraction, t1Magnitude, t2Magnitude);
        }
        double t1 = secondsFraction * t1Magnitude;
        double t2 = secondsFraction * t2Magnitude;

        if (pauseSimulation or (timeBendersPauseTime and withinSomeTimeBender)) {
            // skip simulation
        } else {
            physics.iterate(getCurrentLevel(), character, secondsFraction, t1, t2);
        }
    }

    if (isDead()) {
        auto oldSpeed = character.speed;
        reloadLevel(currentLevelName);
        resetTime();
        character.speed = oldSpeed;
    }

    tryAdvanceLevel();
    auto &particles = getCurrentLevel().particles;
    auto halfsize = 16.5;
    for (auto &particle : particles) {
        if(particle.getConstPosition().y < -halfsize) {
            particle.getPosition().y += halfsize*2;
        }
        if(particle.getConstPosition().x < -halfsize) {
            particle.getPosition().x += halfsize*2;
        }
        if(particle.getConstPosition().x > halfsize) {
            particle.getPosition().x -= halfsize*2;
        }
        if(particle.getConstPosition().z < -halfsize) {
            particle.getPosition().z += halfsize*2;
        }
        if(particle.getConstPosition().z > halfsize) {
            particle.getPosition().z -= halfsize*2;
        }
    }
}

TimeBenders Manager::timeBendersAffectingCharacter() {
    TimeBenders affecting;
    for (const auto &timeBender : getCurrentLevel().timeBenders) {
        if (cubesIntersect(character.position, character.size / 2.0, timeBender.centerPosition,
                timeBender.influenceSize / 2.0)) {
            affecting.push_back(timeBender);
        }
    }
    return affecting;
}

bool Manager::isDead() {
    return character.position.y < -50;
}

void Manager::tryAdvanceLevel() {
    if (character.position.y > 80) {
        currentLevelName = getCurrentLevel().goal.nextLevel;
        if (currentLevelName.empty()) {
            currentLevelName = predefinedLevels::LEVEL_1;
        }
        reloadLevel(currentLevelName);
        resetTime();
    }
}

void Manager::onKeyChange(SDL_KeyboardEvent &event) {
    auto boolToString = [](bool b) -> std::string {
        return b ? "true" : "false";
    };
    if (event.state == SDL_PRESSED) {
        if (ui.isDebugging()) {
            switch (event.keysym.sym) {
            case SDLK_c:
                freeCamera = not freeCamera;
                tryGetWindow().setWindowTitle(std::string{"c: control camera or character: "}
                        + (freeCamera ? "camera" : "character"));
                break;
            case SDLK_r:
                reloadLevel(currentLevelName);
                tryGetWindow().setWindowTitle("r: level was reset");
                break;
            case SDLK_t:
                periodicRotation = not periodicRotation;
                tryGetWindow().setWindowTitle("t: periodic rotation: " + boolToString(periodicRotation));
                break;
            case SDLK_g:
                tryGetWindow().setWindowTitle("g: gravity enabled: " + boolToString(physics.toggleGravity()));
                break;
            case SDLK_F3:
                ui.toggleDebug();
                break;
            case SDLK_e:
                physics.setMaxPlayerSpeed(physics.getMaxPlayerSpeed() * 3);
                break;
            case SDLK_q:
                physics.setMaxPlayerSpeed(physics.getMaxPlayerSpeed() / 3);
                break;
            case SDLK_p:
                pauseSimulation = not pauseSimulation;
                tryGetWindow().setWindowTitle("space: simulation paused: " + boolToString(pauseSimulation));
                break;
            }
        } else {
            switch (event.keysym.sym) {
            case SDLK_F3:
                ui.toggleDebug();
                break;
            }
        }

        if (freeCamera) {
            switch (event.keysym.sym) {
            case SDLK_SPACE:
                pauseSimulation = not pauseSimulation;
                tryGetWindow().setWindowTitle("space: simulation paused: " + boolToString(pauseSimulation));
                break;
            }
        } else {
            switch (event.keysym.sym) {
            case SDLK_SPACE:
                if (not character.inAir) {
                    character.inAir = true;
                    character.speed += Vector3D{0, 10, 0};
                    tryGetWindow().setWindowTitle("space: jumping");
                } else {
                    tryGetWindow().setWindowTitle("space: can not jump in the air");
                }
                break;
            }
        }
    }
}

void Manager::onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) {
    auto dir = vldr.getDir();
    dir.y = 0;
    auto forwardSpeed = dir.Unit() * physics.getMaxPlayerSpeed() / 5.0;

    auto left = vldr.getX();
    auto leftSpeed = left.Unit() * physics.getMaxPlayerSpeed() / 5.0;

    if (freeCamera) {
        FlyerManagerBase::onKeyPressed(keyCode, keyMod, scanCode);
    } else {
        if (ui.isDebugging()) {
            switch (keyCode) {
            case SDLK_l:
                bendTime(TimeRotation::TOWARDS_T1, 0.03, t1Magnitude, t2Magnitude);
                break;
            case SDLK_j:
                bendTime(TimeRotation::AWAY_FROM_T1, 0.03, t1Magnitude, t2Magnitude);
                break;
            case SDLK_i:
                bendTime(TimeRotation::TOWARDS_T2, 0.03, t1Magnitude, t2Magnitude);
                break;
            case SDLK_k:
                bendTime(TimeRotation::AWAY_FROM_T2, 0.03, t1Magnitude, t2Magnitude);
                break;
            case SDLK_m:
                resetTime();
                t1Magnitude *= -1;
                t2Magnitude *= -1;
                break;
            case SDLK_u:
                t1Magnitude = 0;
                t2Magnitude = 1;
                break;
            case SDLK_o:
                resetTime();
                break;
            case SDLK_SEMICOLON:
                t1Magnitude = 1;
                t2Magnitude = 0;
                break;
            default:
                break;
            }
        }
        switch (keyCode) {
        case SDLK_w:
            if (not character.inAir) {
                character.speed += forwardSpeed;
            }
            break;
        case SDLK_a:
            if (not character.inAir) {
                character.speed += leftSpeed;
            }
            break;
        case SDLK_s:
            if (not character.inAir) {
                character.speed -= forwardSpeed;
            }
            break;
        case SDLK_d:
            if (not character.inAir) {
                character.speed -= leftSpeed;
            }
            break;
        default:
            break;
        }
    }
}

void Manager::onMouseMotion(SDL_MouseMotionEvent &event) {
    FlyerManagerBase::onMouseMotion(event);
    cameraOffset = vldr.getPos() - center;
    ui.onClick(randomize::UI::Event::UI_EVENT_MOVE, event.x, event.y);
}

void Manager::onMouseWheel(SDL_MouseWheelEvent &event) {
    FlyerManagerBase::onMouseWheel(event);
    cameraOffset = vldr.getPos() - center;
}

void Manager::onMouseButton(SDL_MouseButtonEvent &event) {
    FlyerManagerBase::onMouseButton(event);
    auto clickOrUnclick =
            event.state == SDL_PRESSED ? randomize::UI::Event::UI_EVENT_DOWN : randomize::UI::Event::UI_EVENT_UP;
    ui.onClick(clickOrUnclick, event.x, event.y);
}
