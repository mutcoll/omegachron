/**
 * @file Levels.h
 */

#ifndef OMEGACHRON_LEVELS_H
#define OMEGACHRON_LEVELS_H

#include <vector>
#include <map>
#include <entities/Platform.h>
#include "graphics/Geometry.h"
#include "entities/Particle.h"
#include "entities/TimeBender.h"


struct Level {
    struct Goal {
        Vector3D center;
        double size = 3;
        std::string nextLevel;
    };

    Particles particles;
    std::vector<Quad> planes;
    TimeBenders timeBenders;
    Platforms platforms;
    Goal goal;
};

using Levels = std::map<std::string, Level>;

void addPredefinedLevels(Levels &levels);

namespace predefinedLevels {
const std::string CLOCKS_TEST_LEVEL = "CLOCKS_TEST_LEVEL";
const std::string PARTICLES_TEST_LEVEL = "PARTICLES_TEST_LEVEL";
const std::string CORNER_TEST_LEVEL = "CORNER_TEST_LEVEL";
const std::string FRICTION_TEST_LEVEL = "FRICTION_TEST_LEVEL";
const std::string RAIN_TEST_LEVEL = "RAIN_TEST_LEVEL";
const std::string LEVEL_1 = "1";
const std::string LEVEL_2 = "2";
const std::string LEVEL_3 = "3";
const std::string LEVEL_4 = "4";
const std::string LEVEL_5 = "5";
const std::string LEVEL_6 = "6";
}

#endif //OMEGACHRON_LEVELS_H
