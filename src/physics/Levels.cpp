/**
 * @file Levels.cpp
 */


#include "Levels.h"

void addPredefinedLevels(Levels &levels) {
    {
        Level &level = levels[predefinedLevels::CLOCKS_TEST_LEVEL];

        level.timeBenders.push_back(TimeBender{{10, 0, -10}, 1, 8, GREEN, TimeRotation::TOWARDS_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, -10}, 1, 8, ORANGE, TimeRotation::TOWARDS_T2});
        level.timeBenders.push_back(TimeBender{{10, 0, 10}, 1, 8, ANTI_GREEN, TimeRotation::AWAY_FROM_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, 10}, 1, 8, ANTI_ORANGE, TimeRotation::AWAY_FROM_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, -10}, 1, 8, GREEN_ORANGE,
                TimeRotation::TOWARDS_HALFWAY_T1_AND_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, 10}, 1, 8, ANTI_GREEN_ORANGE,
                TimeRotation::AWAY_FROM_HALFWAY_T1_AND_T2});
        level.timeBenders.push_back(TimeBender{{10, 0, 0}, 1, 8, LIME, TimeRotation::ROTATE_FROM_T1_TO_T2});
        level.timeBenders.push_back(TimeBender{{-10, 0, 0}, 1, 8, YELLOW, TimeRotation::ROTATE_FROM_T2_TO_T1});

        level.particles.push_back(BiTimedParticle{Vector3D{2, 3, 2}, Vector3D{5, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-4, 3, -4}, Vector3D{0, 0, 0}, Vector3D{0, 0, 15}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-1, 3, -1}, Vector3D{10, 0, 0}, Vector3D{0, 0, 5}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-1, 3, 2}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});

        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, halfSize*2, 0}, {0, 0, -halfSize*2}));
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {-halfSize*2, 0, 0}, {0, halfSize*2, 0}));
        level.planes.push_back(buildPlane({-halfSize, -0.5, -halfSize}, {halfSize*2, 0, 0}, {0, halfSize*2, 0}));
        level.planes.push_back(buildPlane({-halfSize, -0.5, -halfSize}, {0, halfSize*2, 0}, {0, 0, halfSize*2}));

        level.goal.center = {20, 0, 20};
    }

    {
        Level &level = levels[predefinedLevels::PARTICLES_TEST_LEVEL];

        level.timeBenders.push_back(TimeBender{{10, 0, -10}, 1, 8, GREEN, TimeRotation::TOWARDS_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, -10}, 1, 8, ORANGE, TimeRotation::TOWARDS_T2});
        level.timeBenders.push_back(TimeBender{{10, 0, 10}, 1, 8, ANTI_GREEN, TimeRotation::AWAY_FROM_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, 10}, 1, 8, ANTI_ORANGE, TimeRotation::AWAY_FROM_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, -10}, 1, 8, GREEN_ORANGE,
                TimeRotation::TOWARDS_HALFWAY_T1_AND_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, 10}, 1, 8, ANTI_GREEN_ORANGE,
                TimeRotation::AWAY_FROM_HALFWAY_T1_AND_T2});
        level.timeBenders.push_back(TimeBender{{10, 0, 0}, 1, 8, LIME, TimeRotation::ROTATE_FROM_T1_TO_T2});
        level.timeBenders.push_back(TimeBender{{-10, 0, 0}, 1, 8, YELLOW, TimeRotation::ROTATE_FROM_T2_TO_T1});

        level.particles.clear();
        level.particles.push_back(BiTimedParticle{Vector3D{2, 3, 2}, Vector3D{1, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-4, 3, -4}, Vector3D{0, 0, 0}, Vector3D{0, 0, 3}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-1, 3, -1}, Vector3D{2, 0, 0}, Vector3D{0, 0, 1}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{-1, 3, 2}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{1, 30, 1}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 3.5, 2}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 3.5, 5}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 3.5, 8}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 3.5, 11}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 6.5, 2}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 6.5, 5}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 6.5, 8}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 6.5, 11}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{2, 3.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{5, 3.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{8, 3.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{11, 3.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{2, 6.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{5, 6.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{8, 6.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{11, 6.5, 0}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});

        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, halfSize*2, 0}, {0, 0, -halfSize*2}));
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {-halfSize*2, 0, 0}, {0, halfSize*2, 0}));
        level.planes.push_back(buildPlane({-halfSize, -0.5, -halfSize}, {halfSize*2, 0, 0}, {0, halfSize*2, 0}));
        level.planes.push_back(buildPlane({-halfSize, -0.5, -halfSize}, {0, halfSize*2, 0}, {0, 0, halfSize*2}));

        level.goal.center = {20, 0, 20};
    }

    {
        Level &level = levels["???"];

        level.timeBenders.push_back(TimeBender{{10, 0, -10}, 1, 8, GREEN, TimeRotation::TOWARDS_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, -10}, 1, 8, ORANGE, TimeRotation::TOWARDS_T2});
        level.timeBenders.push_back(TimeBender{{10, 0, 10}, 1, 8, ANTI_GREEN, TimeRotation::AWAY_FROM_T1});
        level.timeBenders.push_back(TimeBender{{-10, 0, 10}, 1, 8, ANTI_ORANGE, TimeRotation::AWAY_FROM_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, -10}, 1, 8, GREEN_ORANGE,
                TimeRotation::TOWARDS_HALFWAY_T1_AND_T2});
        level.timeBenders.push_back(TimeBender{{0, 0, 10}, 1, 8, ANTI_GREEN_ORANGE,
                TimeRotation::AWAY_FROM_HALFWAY_T1_AND_T2});

        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*4, 0, 0}));

        auto platformSize = 8.0;
        level.platforms.push_back(
                Platform{{20.5, -1.0, 0}, platformSize, 1, platformSize, {0, 0, halfSize - platformSize / 2}, 10,
                        TimeRotation::TOWARDS_T1});

        level.platforms.push_back(
                Platform{{20.5, -1.0, halfSize - platformSize / 2}, platformSize, 1, platformSize, {15, 0, 0}, 10,
                        TimeRotation::TOWARDS_T2});
        level.platforms.push_back(
                Platform{{5, 0.5, 5}, platformSize, 1, platformSize, {0, 0, 0}, 10,
                        TimeRotation::TOWARDS_T2});
        level.platforms.push_back(
                Platform{{-10, 0, 5}, platformSize, platformSize, platformSize, {0, 0, -10}, 10,
                        TimeRotation::TOWARDS_T2});
//
//        level.platforms.push_back(
//                Platform{{0, 2, 0}, platformSize, platformSize, 1, {0, 0, 0}, 100,
//                        TimeRotation::TOWARDS_T2});
//
//        level.particles.push_back(BiTimedParticle{Vector3D{30, 1.1, 0}, Vector3D{0.1, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0, 1.1, 0}, Vector3D{10, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0, 1.1, 4.1}, Vector3D{10, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0, 1.1, -4.1}, Vector3D{10, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0, 6, 0.1}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0.1, 10, 0.1}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
//        level.particles.push_back(BiTimedParticle{Vector3D{0, 2, 6}, Vector3D{0, 0, -0.5}, Vector3D{0, 0, 0}, 1, 1});

    }

    {
        Level &level = levels[predefinedLevels::CORNER_TEST_LEVEL];

        auto platformSize = 8.0;
        auto position = Vector3D{0, -4, 0};
        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({10, 0, 0}, {0, 0, -halfSize*2}, {0, -halfSize*2, 0}));

        level.platforms.push_back(
                Platform{position, platformSize, platformSize, 1, {1, 0, 0}, 100,
                        TimeRotation::TOWARDS_T2});

        level.particles.push_back(BiTimedParticle{Vector3D{10, 5, 0.2}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{10.05, 5, -4, }, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});

        level.particles.push_back(BiTimedParticle{Vector3D{4.2, 2, 0.6}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});
        level.particles.push_back(BiTimedParticle{Vector3D{0, 2, 0.6}, Vector3D{0, 0, 0}, Vector3D{0, 0, 0}, 1, 1});

    }
    {
        Level &level = levels[predefinedLevels::FRICTION_TEST_LEVEL];

        auto platformSize = 8.0;
        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        level.planes.push_back(buildPlane({5, 2.5, 5}, {0, 0, platformSize}, {platformSize, 0, 0}));

        auto platformTravel = 10.0;
        level.platforms.push_back(
                Platform{{5, 0, -platformTravel}, platformSize, 1, platformSize, {0, 0, platformTravel}, 3,
                        TimeRotation::TOWARDS_T1});
        level.platforms.push_back(
                Platform{{5, 1, platformTravel}, platformSize, 1, platformSize, {0, 0, -platformTravel}, 3,
                        TimeRotation::TOWARDS_T2});

        level.platforms.push_back(
                Platform{{0, 3, 5}, platformSize, 1, platformSize, {0, -5, 0}, 3, TimeRotation::TOWARDS_T1});

        level.goal.center = {15, 0,15};
    }

    {
        Level &level = levels[predefinedLevels::RAIN_TEST_LEVEL];

        auto halfSize = 16.5;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        auto phi = 0.5 + sqrt(5) / 2;
        auto phiImpostor = -1 + sqrt(2);
        for (size_t i = 0; i < 80; ++i) {
            auto scatter1 = i * halfSize * phi;
            auto scatter2 = i * halfSize * phiImpostor;
            auto x = (long) floor(scatter1) % ((long) halfSize * 2) + remainder(scatter1, halfSize) - halfSize;
            auto z = (long) floor(scatter2) % ((long) halfSize * 2) + remainder(scatter2, halfSize) - halfSize;
            level.particles.push_back(BiTimedParticle{{x, 20,  z}, {10, 0, 2}, {}, 0.2, 1});
        }

        level.goal.center = {15, 0, 15};
    }

    {
        Level &level = levels[predefinedLevels::LEVEL_1];

        auto platformSize = 8.0;
        auto halfSize = 16.5;
        auto corridorHalfSize = 3.5;
        auto corridorLength = 100.0;
        auto lastPlaneEnd = halfSize;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        lastPlaneEnd = level.planes.back().p[1].z;

        level.planes.push_back(buildPlane({corridorHalfSize, -0.5, lastPlaneEnd}, {0, 0, -corridorLength},
                {-corridorHalfSize * 2, 0, 0}));
        lastPlaneEnd = level.planes.back().p[1].z;

        level.timeBenders.push_back(TimeBender{{0, 0, -40}, 1, 8, GREEN, TimeRotation::TOWARDS_T1});

        level.platforms.push_back(
                Platform{{8, 4, -30}, 1, platformSize, platformSize, {0, 0, -60}, 5, TimeRotation::TOWARDS_T2});
        level.platforms.push_back(
                Platform{{-8, 4, -30}, 1, platformSize, platformSize, {0, 0, -60}, 5, TimeRotation::TOWARDS_T1});

        level.timeBenders.push_back(TimeBender{{0, 0, -80}, 1, 8, ORANGE, TimeRotation::TOWARDS_T2});

        level.planes.push_back(buildPlane({halfSize, -0.5, lastPlaneEnd}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        lastPlaneEnd = level.planes.back().p[1].z;


        level.goal.center = {0, 0, lastPlaneEnd + halfSize};
        level.goal.nextLevel = predefinedLevels::LEVEL_2;
    }

    {
        Level &level = levels[predefinedLevels::LEVEL_2];

        auto platformSize = 8.0;
        auto halfSize = 16.5;
//        auto corridorHalfSize = 3.5;
//        auto corridorLength = 100.0;
        auto lastPlaneEnd = halfSize;
        level.planes.push_back(buildPlane({halfSize, -0.5, halfSize}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        lastPlaneEnd = level.planes.back().p[1].z;

        level.timeBenders.push_back(TimeBender{{8, 0, -8}, 1, 8, GREEN, TimeRotation::TOWARDS_T1});
        level.timeBenders.push_back(TimeBender{{-8, 0, -8}, 1, 8, ORANGE, TimeRotation::TOWARDS_T2});

        auto platformSeparation = 30.0;
        level.platforms.push_back(
                Platform{{0, 0, lastPlaneEnd}, platformSize, 1, platformSize, {0, 0, -platformSeparation}, 5,
                        TimeRotation::TOWARDS_T1});
//        level.platforms.push_back(Platform{{0, 1, lastPlaneEnd - platformSeparation}, platformSize, 1, platformSize,
//                {0, 0, -platformSeparation}, 5, TimeRotation::TOWARDS_T1});
        level.platforms.push_back(Platform{{0, 1, lastPlaneEnd - 2*platformSeparation}, platformSize, 1, platformSize,
                {0, 0, platformSeparation}, 5, TimeRotation::TOWARDS_T1});

        level.platforms.push_back(Platform{{0, 5, 5}, platformSize, 1, platformSize,
                {0, -10, 0}, 5, TimeRotation::TOWARDS_T1});

        level.planes.push_back(buildPlane({halfSize, +0.5, lastPlaneEnd - 57}, {0, 0, -halfSize*2}, {-halfSize*2, 0, 0}));
        lastPlaneEnd = level.planes.back().p[1].z;

        level.goal.center = {0, 1, lastPlaneEnd + halfSize};
    }
}

