/**
 * @file Physics.h
 */

#ifndef PARTICLEAUTOMATON_PHYSICS_H
#define PARTICLEAUTOMATON_PHYSICS_H


#include <vector>
#include "entities/Particle.h"
#include "graphics/Geometry.h"
//#include "math/Geom.h"
//#include "manager_templates/FlyerManagerBase.h"

extern double extraBounceSpeed;

/**
 * All times are expressed in seconds
 */
class Physics {
public:
    Physics(bool gravityEnabled, double maxPlayerSpeed);

    void iterate(Level &level, Character &character, double t, double t1, double t2);
    void iterate(Platform &platform, double t1, double t2);
    void iterate(BiTimedParticle &particle, double t1, double t2);
    void iterate(Character &character, double seconds, Vector3D accum);

    void interaction(Particle &particle, const Platform &platform, double particleSeconds, double planeSeconds);
    Vector3D interaction(Particle &particle, const Quad &plane, double particleSeconds);
    Vector3D interaction(Particle &particle, const Quad &plane, const Vector3D &planeSpeed, double particleSeconds,
            double planeSeconds);

    void interaction(Particle &first, Particle &second, double seconds);
    void bounceParticles(Particle &first, Particle &second, double seconds);
    void elasticCollision(const Vector3D &firstSpeed, const Vector3D &secondSpeed,
            double firstMass, double secondMass,
            Vector3D &newFirstSpeed, Vector3D &newSecondSpeed) const;

    Vector3D interaction(Character &character, const Platform &platform, double seconds, double t1,
            double t2);
    Vector3D interaction(Character &character, const Quad &plane, const Vector3D &planeSpeed,
            double characterSeconds, double planeSeconds);

    bool toggleGravity() {
        gravityEnabled = not gravityEnabled;
        return gravityEnabled;
    }

    double getMaxPlayerSpeed() const;
    void setMaxPlayerSpeed(double maxPlayerSpeed);

private:
    bool gravityEnabled;
    double maxPlayerSpeed;
};


#endif //PARTICLEAUTOMATON_PHYSICS_H
