/**
 * @file Physics.cpp
 */

//#include "Manager.h"
#include <vector>
#include <randomize/utils/exception/StackTracedException.h>
#include "graphics/drawers.h"
#include "Manager.h"
#include "graphics/Geometry.h"
//#include "math/Geom.h"
#include "Physics.h"


const auto GRAVITY_ACCELERATION = Vector3D{0, -30, 0};
double extraBounceSpeed = 0;

Physics::Physics(bool gravityEnabled, double maxPlayerSpeed)
        : gravityEnabled(gravityEnabled), maxPlayerSpeed(maxPlayerSpeed) {}

void Physics::iterate(Level &level, Character &character, double t, double t1, double t2) {
    auto &particles = level.particles;
    for (size_t i = 0; i < particles.size(); ++i) {
        auto &particle = particles[i];
        iterate(particle, t1, t2);
        for (auto &plane : level.planes) {
            interaction(particle.firstParticleView, plane, t1);
            interaction(particle.secondParticleView, plane, t2);
        }
        for (const auto &platform : level.platforms) {
            auto platformTimeIncrease = (platform.timeRotation == TimeRotation::TOWARDS_T1 ? t1 : t2);
            interaction(particle.firstParticleView, platform, t1, platformTimeIncrease);
            interaction(particle.secondParticleView, platform, t2, platformTimeIncrease);
        }
        for (size_t j = 0; j < particles.size(); ++j) {
            if (j != i) {
                interaction(particle.firstParticleView, particles[j].firstParticleView, t1);
                interaction(particle.secondParticleView, particles[j].secondParticleView, t2);
            }
        }
    }

    auto &platforms = level.platforms;
    for (auto &platform : platforms) {
        iterate(platform, t1, t2);
    }

    character.inAir = true;
    Vector3D accum;
    for (auto &plane : level.planes) {
        interaction(character, plane, {0, 0, 0}, t, 1);
    }
    for (const auto &platform : level.platforms) {
        accum += interaction(character, platform, t, t1, t2);
    }
    if (cubesIntersect(character.position, character.size / 2, level.goal.center, level.goal.size / 2)) {
        character.speed.y = 100;
    }
    iterate(character, t, accum);
}

void Physics::iterate(Platform &platform, double t1, double t2) {
    auto seconds = platform.timeRotation == TimeRotation::TOWARDS_T1 ? t1 : t2;

    auto unitaryPositionBefore = -cos(platform.instant);
    auto distance = platform.animationDirection.Modulo();
    auto speed = distance / platform.period;
    auto returning = int(std::abs(platform.instant) / platform.period) % 2 == 1;
    if (returning) {
        if (platform.instant >= 0) {
            speed = -speed;
        }
    } else {
        if (platform.instant < 0) {
            speed = -speed;
        }
    }

    auto unitaryDirection = platform.animationDirection / distance;
    platform.center += unitaryDirection * speed * seconds;
    platform.speed = unitaryDirection * speed;
    platform.instant += seconds;
}

void Physics::iterate(BiTimedParticle &particle, double t1, double t2) {
    Vector3D gravityAcceleration = gravityEnabled ? GRAVITY_ACCELERATION : Vector3D{0, 0, 0};
    particle.getPosition() += 0.5 * gravityAcceleration * t1 * t1 + particle.getConstSpeed1() * t1
            + 0.5 * gravityAcceleration * t2 * t2 + particle.getConstSpeed2() * t2;
    particle.getSpeed1() += gravityAcceleration * t1;
    particle.getSpeed2() += gravityAcceleration * t2;
}

void Physics::iterate(Character &character, double seconds, Vector3D accum) {
    Vector3D acceleration = gravityEnabled ? GRAVITY_ACCELERATION : Vector3D{0, 0, 0};
//    acceleration += accum * 5.5;
    acceleration += character.acceleration;
    auto speed = character.speed + accum;
    character.position += 0.5 * acceleration * seconds * seconds + speed * seconds;
    character.speed += acceleration * seconds;
}

//////////////// platform/plane - particle

void Physics::interaction(Particle &particle, const Platform &platform, double particleSeconds, double planeSeconds) {
    for (const auto &plane : platform.getPlanes()) {
        interaction(particle, plane, platform.speed, particleSeconds, planeSeconds);
    }
}

Vector3D Physics::interaction(Particle &particle, const Quad &plane, double particleSeconds) {
    return interaction(particle, plane, {0, 0, 0}, particleSeconds, 1);
}

Vector3D Physics::interaction(Particle &particle, const Quad &plane, const Vector3D &planeSpeed,
        double particleSeconds, double planeSeconds) {
    if (particleSeconds == 0) {
        return {};
    }
    auto bounceLoss = 0.5;
    double distanceFromPlaneToSphereCenter = 0;
    Vector3D accum;
    Vector3D projectionOverPlane;
    Vector3D projection;
    auto insidePlane = fallsInsideConvexPlane(plane, particle.getConstPosition(), distanceFromPlaneToSphereCenter,
            projectionOverPlane);
    if (std::abs(distanceFromPlaneToSphereCenter) < particle.getConstRadius()) {
        if (not insidePlane) {
            auto intersections = collectEdgeIntersections(particle.getConstPosition(), particle.getConstRadius(),
                    plane);
            if (not intersections.empty()) {
                if (intersections.size() != 2) {
                    throw randomize::utils::exception::StackTracedException(std::to_string(intersections.size())
                            + " is an impossible number of particle-plane intersections");
                } else {
                    double distanceFromPointInTheLineToProjection;
                    auto projectionOverPlaneEdge = getPointProjectionOverLine(particle.getConstPosition(),
                            intersections[0], (intersections[1] - intersections[0]).Unit(),
                            distanceFromPointInTheLineToProjection);

                    projection = projectionOverPlaneEdge;
                }
            } else {
                // at the same height than the plane, but not touching it
                return accum;
            }
        } else {
            projection = projectionOverPlane;
        }

        auto distance = (particle.getConstPosition() - projection).Modulo();

        bool approachingPlane;
        auto planeToCharacterUnit = (particle.getConstPosition() - projection).Unit();
        auto speedDifference = particle.getConstSpeed() * particleSeconds - planeSpeed * planeSeconds;
        Vector3D normalSpeed;
        double speedDifferenceProyectionOverNormalNegativeIfApproaching = speedDifference * planeToCharacterUnit;
        if (speedDifferenceProyectionOverNormalNegativeIfApproaching < 0
                and std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching) < particleSeconds) {
            // inelastic collision to stand still in the floor without bouncing eternally
            // nor slowly breaking through the plane
            auto overlap = particle.getConstRadius() - std::abs(distance);
            auto squaredOverlap = overlap * overlap;
            auto cubedOverlap = squaredOverlap * overlap;
            auto squaredSpeedDifferenceProjection = std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching)
                    * std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching);
            auto extraBounce = (overlap + std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching));
            auto timeDependentCoefficient = -2700000 * particleSeconds * particleSeconds + 36000 * particleSeconds;
            particle.getSpeed() += planeToCharacterUnit * extraBounce * timeDependentCoefficient;
            approachingPlane = true;
            normalSpeed = {0, 0, 0};
        } else {
            approachingPlane = speedDifferenceProyectionOverNormalNegativeIfApproaching < 0;
            double bounce;
            if (particleSeconds >= 0) {
                bounce = 2 - bounceLoss;
            } else {
                bounce = (2 - bounceLoss) / (1 - bounceLoss);
            }
            normalSpeed = planeToCharacterUnit * -speedDifferenceProyectionOverNormalNegativeIfApproaching
                    / particleSeconds * bounce;
        }
        if (approachingPlane) {
            particle.getSpeed() += normalSpeed; // bounce

            auto planarSpeedDifference = speedDifference - speedDifference * planeToCharacterUnit * planeToCharacterUnit;
            particle.getSpeed() -= planarSpeedDifference * 10;  // friction
        }
    }
    return accum;
}


//////////////// particle - particle

void Physics::interaction(Particle &first, Particle &second, double seconds) {
    if (spheresIntersect(first.getConstPosition(), first.getConstRadius(), second.getConstPosition(), second.getConstRadius())) {
        bounceParticles(first, second, seconds);
    }
}

void Physics::bounceParticles(Particle &first, Particle &second, double seconds) {
    Vector3D distance = second.getConstPosition() - first.getConstPosition();
    if (distance.Modulo() == 0) {
        LOG_WARN("Trying to bounce two particles with the same position! skipping bouncing...")
    } else {
        Vector3D normal = distance.Unit();

        double timeDirection = seconds >= 0 ? 1 : -1;
        float firstSpeedMagnitudeOverNormal = first.getConstSpeed() * normal;
        float secondSpeedMagnitudeOverNormal = second.getConstSpeed() * normal;
        if (firstSpeedMagnitudeOverNormal * timeDirection < 0 and secondSpeedMagnitudeOverNormal * timeDirection > 0) {
            // the particles are moving away from each other, let them do that without bouncing inwards
            return;
        }
        Vector3D firstNormal = firstSpeedMagnitudeOverNormal * normal;
        Vector3D secondNormal = secondSpeedMagnitudeOverNormal * normal;

        Vector3D newFirstSpeed;
        Vector3D newSecondSpeed;
        elasticCollision(
                firstNormal, secondNormal,
                first.getConstMass(), second.getConstMass(),
                newFirstSpeed, newSecondSpeed);

        newFirstSpeed += first.getConstSpeed() - firstNormal;
        newSecondSpeed += second.getConstSpeed() - secondNormal;

        first.getSpeed() = newFirstSpeed;
        second.getSpeed() = newSecondSpeed;
    }
}

/**
 * https://en.wikipedia.org/wiki/Elastic_collision#One-dimensional_Newtonian
 */
void Physics::elasticCollision(const Vector3D &firstSpeed, const Vector3D &secondSpeed,
        double firstMass, double secondMass,
        Vector3D &newFirstSpeed, Vector3D &newSecondSpeed) const {

    double masses = firstMass + secondMass;
    Vector3D firstMomentum = firstMass * firstSpeed;
    Vector3D secondMomentum = secondMass * secondSpeed;

    newFirstSpeed = (firstSpeed * (firstMass - secondMass) + 2 * secondMomentum) / masses;
    newSecondSpeed = (secondSpeed * (secondMass - firstMass) + 2 * firstMomentum) / masses;
}


//////////////// character - plane

Vector3D Physics::interaction(Character &character, const Platform &platform, double seconds, double t1,
        double t2) {
    Vector3D accum;
    for (const auto &plane : platform.getPlanes()) {
        auto timeIncrease = (platform.timeRotation == TimeRotation::TOWARDS_T1 ? t1 : t2);
        accum += interaction(character, plane, platform.speed, seconds, timeIncrease);
    }
    return accum;
}

Vector3D Physics::interaction(Character &character, const Quad &plane, const Vector3D &planeSpeed,
        double characterSeconds, double planeSeconds) {
    auto bounceLoss = 0.9;
    double distanceFromPlaneToSphereCenter = 0;
    Vector3D accum;
    Vector3D projectionOverPlane;
    Vector3D projection;
    auto insidePlane = fallsInsideConvexPlane(plane, character.position, distanceFromPlaneToSphereCenter, projectionOverPlane);
    if (std::abs(distanceFromPlaneToSphereCenter) < character.size/2.0) {
        if (not insidePlane) {
            auto intersections = collectEdgeIntersections(character.position, character.size / 2,
                    plane);
            if (not intersections.empty()) {
                if (intersections.size() != 2) {
                    throw randomize::utils::exception::StackTracedException(std::to_string(intersections.size())
                            + " is an impossible number of particle-plane intersections");
                } else {
                    double distanceFromPointInTheLineToProjection;
                    auto projectionOverPlaneEdge = getPointProjectionOverLine(character.position,
                            intersections[0], (intersections[1] - intersections[0]).Unit(),
                            distanceFromPointInTheLineToProjection);

                    projection = projectionOverPlaneEdge;
                }
            } else {
                // at the same height than the plane, but not touching it
                return accum;
            }
        } else {
            projection = projectionOverPlane;
        }

        auto distance = (character.position - projection).Modulo();

        bool approachingPlane;
        auto planeToCharacterUnit = (character.position - projection).Unit();
        auto speedDifference = character.speed * characterSeconds - planeSpeed * planeSeconds;
        Vector3D normalSpeed;
        double speedDifferenceProyectionOverNormalNegativeIfApproaching = speedDifference * planeToCharacterUnit;
        if (speedDifferenceProyectionOverNormalNegativeIfApproaching < 0
                and std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching) < characterSeconds) {
            // inelastic collision to stand still in the floor without bouncing eternaly nor slowly breaking through the plane
            auto overlap = character.size / 2.0 - std::abs(distance);
            auto extraBounce = (overlap + std::abs(speedDifferenceProyectionOverNormalNegativeIfApproaching));
            auto timeDependentCoefficient = -2700000 * characterSeconds * characterSeconds + 36000 * characterSeconds;
            character.speed += planeToCharacterUnit * extraBounce * timeDependentCoefficient;
            approachingPlane = true;
            normalSpeed = {0, 0, 0};
        } else {
            approachingPlane = speedDifferenceProyectionOverNormalNegativeIfApproaching < 0;
            normalSpeed = planeToCharacterUnit * -speedDifferenceProyectionOverNormalNegativeIfApproaching
                    / characterSeconds * (2 - bounceLoss);
        }
        character.inAir = false;
        if (approachingPlane) {
            character.speed += normalSpeed; // bounce

            auto planarSpeedDifference = speedDifference - speedDifference * planeToCharacterUnit * planeToCharacterUnit;
            character.speed -= planarSpeedDifference * 10;  // friction
        }
    }
    return accum;
}

double Physics::getMaxPlayerSpeed() const {
    return maxPlayerSpeed;
}

void Physics::setMaxPlayerSpeed(double maxPlayerSpeed) {
    Physics::maxPlayerSpeed = maxPlayerSpeed;
}
