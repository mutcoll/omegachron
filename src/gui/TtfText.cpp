/**
 * @file TtfText.cpp
 */

#include "TtfText.h"


bool TtfText::ttfInitialized = false;
bool TtfText::ttfFinalised = false;

TTF_Font *TtfText::font = NULL;

TtfText::TtfText() : color{1.0, 1.0, 1.0, 1.0}, backgroundColor{0.2, 0.2, 0.2, 0.6}, letterWidthCoefficient{1.0} {
    background = true;
    textureId = -1;
    textureIdNeedsUpdate = true;
    alignment = TO_THE_LEFT;
}

void TtfText::Draw() {
    Element::Draw();

    glPushMatrix();
    {
        GLfloat vertices[]{
                -0.5, 0.5, -1,
                0.5, 0.5, -1,
                0.5,-0.5, -1,
                -0.5,-0.5, -1
        };

        GLfloat texturePoints[]{
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f
        };

        int textWidth, textHeight;
        TTF_SizeText(font, sentence.c_str(), &textWidth, &textHeight);
        switch (alignment) {
        case CENTERED:
            glTranslatef(pos_x, pos_y, 0);
            break;
        case TO_THE_LEFT:
            glTranslatef(pos_x + tam_w * textWidth * letterWidthCoefficient / POINT_SIZE / 2, pos_y, 0);
            break;
        case TO_THE_RIGHT:
            glTranslatef(pos_x - tam_w * textWidth * letterWidthCoefficient / POINT_SIZE / 2, pos_y, 0);
            break;
        default:
            throw new randomize::utils::exception::StackTracedException{"TtfText::Alignment case not handled: "
                    + std::to_string(alignment)};
        }
        glScalef(tam_w * textWidth * letterWidthCoefficient / POINT_SIZE, tam_h * textHeight / POINT_SIZE, 1);

        if (textureIdNeedsUpdate) {
            textureIdNeedsUpdate = false;
            if (textureId != -1) {
                glDeleteTextures(1, &textureId);
            }
            textureId = textToTexture(font, color[0] * 255, color[1] * 255, color[2] * 255, sentence.c_str());
        } else {
            glBindTexture(GL_TEXTURE_2D, textureId);
        }

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendEquation(GL_ADD);
        glEnableClientState(GL_VERTEX_ARRAY);
        glDisable(GL_TEXTURE_2D);
        if (background) {
            glColor4fv(backgroundColor);
            glVertexPointer(3, GL_FLOAT, 0, vertices);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        }

        glEnable(GL_TEXTURE_2D);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        glColor4fv(color);
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glTexCoordPointer(2, GL_FLOAT, 0, texturePoints);
        glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );


        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }
    glPopMatrix();

    //TODO UI::BeginDraw sets up this color and all elements assume this is not changed
    // so this has to be reset so no other element is affected by this class' colors
    GLfloat resetColors[] {1.0, 1.0, 1.0, 1.0};
    glColor4fv(resetColors);
}

void TtfText::setSentence(const std::string &sentence) {
    TtfText::sentence = sentence;
    textureIdNeedsUpdate = true;
}

const std::string &TtfText::getSentence() const {
    return sentence;
}

const GLfloat * const TtfText::getColor() const {
    return color;
}

void TtfText::setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    color[0] = r;
    color[1] = g;
    color[2] = b;
    color[3] = a;
    textureIdNeedsUpdate = true;
}

const GLfloat * const TtfText::getBackgroundColor() const {
    return backgroundColor;
}

void TtfText::setBackgroundColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    backgroundColor[0] = r;
    backgroundColor[1] = g;
    backgroundColor[2] = b;
    backgroundColor[3] = a;
}

bool TtfText::isBackgroundEnabled() const {
    return background;
}

void TtfText::setBackground(bool background) {
    TtfText::background = background;
}

TtfText::Alignment TtfText::getAlignment() const {
    return alignment;
}

void TtfText::setAlignment(TtfText::Alignment alignment) {
    TtfText::alignment = alignment;
}
