/**
 * @file Gui.cpp
 */

#include "Gui.h"
#include <sstream>


OmegachronUi::OmegachronUi() {
    auto compassImg = LoadPNG("resources/compass.png");
    randomize::UI::Button::Conf compassConf = {compassImg, 2, 2, 0, 0, 0, 0, 30, 30};
    randomize::UI::Button::Conf arrowConf = {compassImg, 2, 2, 1, 0, 1, 0, 30, 30};
    compass = randomize::UI::Button{compassConf};
    arrow = randomize::UI::Button{arrowConf};
    uiPanel.addElemento(&compass, 80704);
    uiPanel.addElemento(&arrow, 45503);

    debug = true;
}

void OmegachronUi::setupDebugUi(int width, int height, const Character &character, const Level &level) {
    debugGui.setupDebugUi(width, height, character, level);
}

void OmegachronUi::drawUi(int width, int height, double t1Magnitude, double t2Magnitude, const Character &character,
        const Level &level) {
    randomize::UI::setConfWindow(width, height);
    uiPanel.setPosPixel(width, 0);
    uiPanel.addPos(-20, -20);
    arrow.setRotation(atan2(t2Magnitude, t1Magnitude) * 360 / (2 * M_PI));
    uiPanel.Draw();
    if (debug) {
        debugGui.drawDebugUi(width, height, t1Magnitude, t2Magnitude, character, level);
    }
}

void OmegachronUi::onClick(randomize::UI::Event event, int x, int y) {
    debugGui.onClick(event, x, y);
}

