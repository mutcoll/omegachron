/**
 * @file DebugGui.h
 */

#ifndef OMEGACHRON_DEBUGGUI_H
#define OMEGACHRON_DEBUGGUI_H

#include <randomize/UI/Boton.h>
#include <randomize/UI/Button.h>
#include <randomize/UI/Element.h>
#include <randomize/UI/Panel.h>
#include <randomize/UI/Slider.h>
#include <randomize/UI/Switch.h>
#include <randomize/UI/Text.h>
#include <randomize/UI/Text_Input.h>
#include <randomize/UI/UI.h>
#include "TtfText.h"
#include "entities/Character.h"
#include "physics/Levels.h"


static const int MAX_DEBUG_LINES = 1000;

static const float LETTER_SIZE = 2.5;

struct DebugValue {
    randomize::UI::Switch toggle;
    TtfText text;
    std::string name;
    int switchIndexInPanel;
    int textIndexInPanel;

    std::function<void(const Character &character, const Level &level)> updateDisplayedValue;

    template<typename T>
    void printValue(T value);
};

template<typename CONTENT>
struct Folder {
    ButtonTtfText button;
    bool unfolded;
    CONTENT content;
};
struct DebugToggles {
    std::vector<DebugValue> toplevel;
    std::vector<Folder<std::vector<DebugValue>>> folders;
};

struct Tuner {
    randomize::UI::Button sliderBackground;
    randomize::UI::Slider slider;
    DebugValue debugValue;
    std::function<void()> updateValue;
};

class DebugGui {
public:
    DebugGui();

    void setupDebugUi(double width, float height, const Character &character, const Level &level);
    void drawDebugUi(int width, int height, double t1Magnitude, double t2Magnitude, const Character &character,
            const Level &level);

    void onClick(randomize::UI::Event event, int x, int y);

private:
    /**
     * @tparam F something like "auto retrieverFunction(const Character &character, const Level &level)"
     */
    template<typename F>
    void setupDebugValue(std::string name, F valueRetriever);

    template<typename F>
    void setupDebugValue(Folder<std::vector<DebugValue>> &folder, std::string name, F valueRetriever);

    template<typename F>
    void setupDebugValue(DebugValue &debugValue, std::string name, F valueRetriever);


    Folder<std::vector<DebugValue>> &addFolder(std::string name, int size);

    template<typename T>
    void addFolder(Folder<std::vector<T>> &folder, std::string name, int id);

    void updatePanelSize(int width, int height);

private:
    DebugToggles debugToggles;
    randomize::UI::Panel<MAX_DEBUG_LINES> uiDebugSelectionPanel;
    randomize::UI::Button::Conf buttonConf;
    randomize::UI::Button::Conf folderConf;
    Tuner tuner;
};


#endif //OMEGACHRON_DEBUGGUI_H
