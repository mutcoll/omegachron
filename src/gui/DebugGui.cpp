/**
 * @file DebugGui.cpp
 */

#include <sstream>
#include "DebugGui.h"
#include "physics/Physics.h"

double tunableValueWithSlider = 0;

template<typename T>
void DebugValue::printValue(T value) {
    if (toggle.getStatus()) {
        std::stringstream ss;
        ss.setf(std::ios::fixed);
        ss.setf(std::ios::showpos);
        ss << name << ": " << value;
        std::string line = ss.str();
        text.setSentence(line);
    } else {
        text.setSentence(name);
    }
}

DebugGui::DebugGui() {
    auto buttonImg = LoadPNG("resources/button.png");
    buttonConf = {buttonImg, 5, 5, 2, 2, 3, 2, 3, 3};
    folderConf = {buttonImg, 1, 5, 0, 0, 0, 1, 16, 3};

    auto lettersImg = LoadPNG("resources/letters.png");
    SpriteChar::setTexture(lettersImg);
    randomize::UI::setTestBox(false);

    if (not TtfText::ttfInitialized) {
        TtfText::initializeFont("resources/OpenSans-Bold.ttf");
    }

    int frames = 16;
    std::vector<unsigned char> rainbow( frames * 4);
    for (int i = 0; i < frames; ++i) {
        rainbow[i * 4 + 0] = 255 / frames * i;
        rainbow[i * 4 + 1] = 255 / frames * i;
        rainbow[i * 4 + 2] = 255 / frames * i;
        rainbow[i * 4 + 3] = 255;
    }
    GLuint imgGradient = LoadImg(rainbow, frames, 1);

    auto imgSlider = LoadPNG("resources/button.png");
    randomize::UI::Button::Conf sliderConf = {imgSlider, 10, 5, 9, 2, 9, 2, 4, 7};

    tuner.sliderBackground.addCapa(0, imgGradient, 1, 1);
    auto sliderWidth = 90.0;
    tuner.sliderBackground.setTam(sliderWidth, 2);
    auto sliderPosX = 0;
    auto sliderPosY = 43;
    tuner.sliderBackground.setPos(sliderPosX, sliderPosY);
    tuner.slider.setPos(sliderPosX, sliderPosY);
    tuner.slider.setConfig(sliderConf);
    tuner.slider.setFree(true, false);
    tuner.slider.setMin(sliderPosX - sliderWidth / 2, sliderPosY);
    tuner.slider.setMax(sliderPosX + sliderWidth / 2, sliderPosY);
    tuner.debugValue.text.setTam(LETTER_SIZE, LETTER_SIZE);
    tuner.debugValue.text.setPos(sliderPosX, sliderPosY - 5);
    tuner.debugValue.text.setAlignment(TtfText::CENTERED);
    tuner.debugValue.toggle.setConfig({LoadImg({0}, 1, 1), 1, 1, 0, 0, 0, 0, 1, 1});
    tuner.debugValue.toggle.setStatus(true);
}

void DebugGui::setupDebugUi(double width, float height, const Character &character, const Level &level) {
    uiDebugSelectionPanel.Limpiar();
    uiDebugSelectionPanel.setTamPixel(width / 2.0, height);
    uiDebugSelectionPanel.addElemento(&tuner.sliderBackground, 0);
    uiDebugSelectionPanel.addElemento(&tuner.slider, 0);
    uiDebugSelectionPanel.addElemento(&tuner.debugValue.text, 0);
    tuner.debugValue.updateDisplayedValue = [=](const Character &character, const Level &level) {
        tuner.debugValue.name = "tunable value: ";
        tuner.debugValue.printValue(tunableValueWithSlider);
    };
    tuner.updateValue = [=](){
        tunableValueWithSlider = pow(10, (tuner.slider.getX() - 0.5) * 8);
    };

    debugToggles.toplevel.clear();
    debugToggles.toplevel.reserve(5);
    debugToggles.folders.clear();
    debugToggles.folders.reserve(100);

    setupDebugValue("character position", [](const Character &character, const Level &level){
        return character.position;
    });
    setupDebugValue("character speed", [](const Character &character, const Level &level){
        return character.speed;
    });
    setupDebugValue("is character in air?", [](const Character &character, const Level &level){
        return character.inAir;
    });

    {
        auto &folder = addFolder("platforms", level.platforms.size() * 2);
        for (size_t i = 0; i < level.platforms.size(); ++i) {
            setupDebugValue(folder, "platform " + std::to_string(i) + " center",
                    [i](const Character &character, const Level &level) {
                        return level.platforms[i].center;
                    });
            setupDebugValue(folder, "platform " + std::to_string(i) + " speed",
                    [i](const Character &character, const Level &level) {
                        return level.platforms[i].speed;
                    });
        }
    }

    {
        auto &folder = addFolder("planes", level.planes.size() * 1);
        for (size_t i = 0; i < level.planes.size(); ++i) {
            setupDebugValue(folder, "plane " + std::to_string(i) + " center",
                    [i](const Character &character, const Level &level) {
                        return (level.planes[i].p[0] + level.planes[i].p[2]) / 2;
                    });
        }
    }

    {
        auto &folder = addFolder("particles", level.particles.size() * 3);
        for (size_t i = 0; i < level.particles.size(); ++i) {
            setupDebugValue(folder, "particle " + std::to_string(i) + " center",
                    [i](const Character &character, const Level &level) {
                        return level.particles[i].getConstPosition();
                    });
            setupDebugValue(folder, "particle " + std::to_string(i) + " speed1",
                    [i](const Character &character, const Level &level) {
                        return level.particles[i].getConstSpeed1();
                    });
            setupDebugValue(folder, "particle " + std::to_string(i) + " speed2",
                    [i](const Character &character, const Level &level) {
                        return level.particles[i].getConstSpeed2();
                    });
        }
    }
}

template<typename F>
void DebugGui::setupDebugValue(std::string name, F valueRetriever) {
    debugToggles.toplevel.emplace_back();
    auto &debugValue = debugToggles.toplevel.back();
    setupDebugValue(debugValue, name, valueRetriever);
}

template<typename F>
void DebugGui::setupDebugValue(Folder<std::vector<DebugValue>> &folder, std::string name, F valueRetriever) {
    folder.content.emplace_back();
    auto &debugValue = folder.content.back();
    setupDebugValue(debugValue, name, valueRetriever);
}

template<typename F>
void DebugGui::setupDebugValue(DebugValue &debugValue, std::string name, F valueRetriever) {
    debugValue.toggle.setConfig(buttonConf);
    debugValue.switchIndexInPanel = uiDebugSelectionPanel.addElemento(&debugValue.toggle, 0);

    debugValue.name = name;

    debugValue.text.setTam(LETTER_SIZE, LETTER_SIZE);
    debugValue.textIndexInPanel = uiDebugSelectionPanel.addElemento(&debugValue.text, 0);

    debugValue.updateDisplayedValue = [=, &debugValue](const Character &character, const Level &level) {
        debugValue.printValue(valueRetriever(character, level));
    };
}

Folder<std::vector<DebugValue>>& DebugGui::addFolder(std::string name, int size) {
    debugToggles.folders.emplace_back();
    auto &folder = debugToggles.folders.back();
    folder.content.reserve(size);
    addFolder(folder, name, 0);
    return folder;
}

template<typename T>
void DebugGui::addFolder(Folder<std::vector<T>> &folder, std::string name, int id) {
    folder.button.setConfig(folderConf);
    folder.button.setSentence(name);
    folder.unfolded = false;
    folder.button.setListener(
            [&](randomize::UI::Event action, int x, int y, randomize::UI::Element *this_element, void *user_data) {
                if (action == randomize::UI::UI_EVENT_DOWN) {
                    folder.unfolded = not folder.unfolded;
                }
            }, nullptr);

    uiDebugSelectionPanel.addElemento(&folder.button, id);
}

void DebugGui::drawDebugUi(int width, int height, double t1Magnitude, double t2Magnitude, const Character &character,
        const Level &level) {
    updatePanelSize(width, height);

    tuner.debugValue.updateDisplayedValue(character, level);
    tuner.updateValue();

    float switchHeight = 35;
    float switchWidth = 0;
    float lineHeight = 3;

    auto moveSwitch = [&switchWidth, &switchHeight, &lineHeight, this](DebugValue &debugValue, bool enabled) {
        uiDebugSelectionPanel.setActivo(debugValue.switchIndexInPanel, enabled);
        uiDebugSelectionPanel.setActivo(debugValue.textIndexInPanel, enabled);
        if (enabled) {
            switchHeight -= lineHeight;
            debugValue.toggle.setPos(switchWidth - 40, switchHeight);
            debugValue.text.setPos(switchWidth - 40 + LETTER_SIZE, switchHeight);
        }
    };

    for (auto &debugValue : debugToggles.toplevel) {
        debugValue.updateDisplayedValue(character, level);
        moveSwitch(debugValue, true);
    }
    for (auto &folder : debugToggles.folders) {
        switchHeight -= lineHeight;
        folder.button.setPos(-33, switchHeight);
        switchWidth = 7;
        int i = 0;
        for (auto &debugValue : folder.content) {
            debugValue.updateDisplayedValue(character, level);
            moveSwitch(debugValue, folder.unfolded);
            i++;
        }
        switchWidth = 0;
    }

    uiDebugSelectionPanel.Draw();
}

void DebugGui::updatePanelSize(int width, int height) {
    uiDebugSelectionPanel.setTamPixel(width / 2.0, height);
    if (width > height) {
        auto quarterWidthInUnits = 100.0 / height * width / 4;
        uiDebugSelectionPanel.setPos(-quarterWidthInUnits, 0);
    } else {
        uiDebugSelectionPanel.setPos(-25, 0);
    }
}

void DebugGui::onClick(randomize::UI::Event event, int x, int y) {
    uiDebugSelectionPanel.CB_Click(event, x, y);
}
