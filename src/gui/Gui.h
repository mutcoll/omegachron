/**
 * @file Gui.h
 */

#ifndef OMEGACHRON_GUI_H
#define OMEGACHRON_GUI_H

#include <randomize/UI/Boton.h>
#include <randomize/UI/Button.h>
#include <randomize/UI/Element.h>
#include <randomize/UI/Panel.h>
#include <randomize/UI/Slider.h>
#include <randomize/UI/Switch.h>
#include <randomize/UI/Text.h>
#include <randomize/UI/Text_Input.h>
#include <randomize/UI/UI.h>
#include <entities/Character.h>
#include <physics/Levels.h>
#include "TtfText.h"
#include "DebugGui.h"

class OmegachronUi {

public:
    OmegachronUi();

    void setupDebugUi(int width, int height, const Character &character, const Level &level);
    void drawUi(int width, int height, double t1Magnitude, double t2Magnitude, const Character &character,
            const Level &level);

    bool toggleDebug() {
        debug = not debug;
        return debug;
    }

    bool isDebugging() { return debug; }

    void onClick(randomize::UI::Event event, int x, int y);

private:
    randomize::UI::Panel<10> uiPanel;
    randomize::UI::Button arrow;
    randomize::UI::Button compass;
    bool debug;
    DebugGui debugGui;
};


#endif //OMEGACHRON_GUI_H
