/**
 * @file TtfText.h
 */

#ifndef OMEGACHRON_TTFTEXT_H
#define OMEGACHRON_TTFTEXT_H

#include <randomize/UI/Element.h>
#include <randomize/UI/Button.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <randomize/utils/exception/StackTracedException.h>

static const int POINT_SIZE = 20;

class TtfText : public randomize::UI::Element {
public:
    enum Alignment {
        CENTERED,   // the position of this element represents the center of the text
        TO_THE_LEFT,    // position represents the middle of the left-most side of the text
        TO_THE_RIGHT    // position represents the middle of the right-most side of the text
    };
    static bool ttfInitialized;
    static bool ttfFinalised;
    static void initializeFont(const std::string& fontPath) {
        if (not ttfInitialized and not ttfFinalised) {
            if (TTF_WasInit() == 0) {
                TTF_Init();
            }
            font = TTF_OpenFont(fontPath.c_str(), POINT_SIZE);
            if (font == NULL) {
                throw randomize::utils::exception::StackTracedException("couldn't load font at \"" + fontPath + "\":"
                        + SDL_GetError());
            }
            ttfInitialized = true;
        }
    }
    static void finaliseFont() {
        if (not ttfFinalised) {
            TTF_CloseFont(font);
            TTF_Quit();
            ttfFinalised = true;
        }
    }

    static GLuint textToTexture(TTF_Font *font, GLubyte r, GLubyte g, GLubyte b, const char *text) {
        if (not ttfInitialized) {
            throw randomize::utils::exception::StackTracedException{"Please call TtfText::initializeFont with a font "
                                                                    "path before using an instance of TtfText"};
        }
        SDL_Color color = {b, g, r, 255};
        SDL_Surface *msg = TTF_RenderText_Blended(font, text, color);
        if (msg == nullptr) {
            throw randomize::utils::exception::StackTracedException{SDL_GetError()};
        }

        // create new texture, with default filtering state (==mipmapping on)
        GLuint tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);

        // disable mipmapping on the new texture
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, msg->w, msg->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, msg->pixels);

        SDL_FreeSurface(msg);
        return tex;
    }

    TtfText();
    virtual ~TtfText() = default;

    void Draw() override;

    void setSentence(const std::string &sentence);
    const std::string &getSentence() const;

    const GLfloat * const getColor() const;
    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

    const GLfloat * const getBackgroundColor() const;
    void setBackgroundColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

    bool isBackgroundEnabled() const;
    void setBackground(bool background);

    Alignment getAlignment() const;

    void setAlignment(Alignment alignment);

protected:
    static TTF_Font *font;
    std::string sentence;
    GLfloat color[4], backgroundColor[4];
    float letterWidthCoefficient;
    bool background;
    Alignment alignment;
    GLuint textureId;
    bool textureIdNeedsUpdate;
};




class ButtonTtfText : public randomize::UI::Button {
private:
    TtfText ttfText;
    static constexpr double LETTER_SIZE_IN_BUTTON = 0.75;

public:
    ButtonTtfText(): Button{},ttfText{} {
        ttfText.setAlignment(TtfText::CENTERED);
        ttfText.setBackground(false);
    }

    void setPos(float x, float y) override {
        Button::setPos(x, y);
        ttfText.setPos(x - tam_h * 0.2, y + tam_h * 0.1);
    }

    void setTam(float w, float h) override {
        Button::setTam(w, h);
        ttfText.setTam(h * LETTER_SIZE_IN_BUTTON, h * LETTER_SIZE_IN_BUTTON);
    }

    void setSentence(const std::string &sentence) {
        ttfText.setSentence(sentence);
    }

    void Draw() {
        Button::Draw();
        ttfText.Draw();
    }
};



//}};//namespace UI

#endif //OMEGACHRON_TTFTEXT_H
