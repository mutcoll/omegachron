/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_MANAGER_H
#define MYTEMPLATEPROJECT_MANAGER_H

#include <randomize/events/Dispatcher.h>
#include <randomize/events/EventCallThis.h>
#include <randomize/manager_templates/FlyerManagerBase.h>
#include "gui/Gui.h"
#include "entities/Character.h"
#include "entities/TimeBender.h"
#include "graphics/WindowGl3DAnd2D.h"
#include "physics/Levels.h"
#include "physics/Physics.h"


class Manager: public FlyerManagerBase{
public:
    Manager( Dispatcher &d, std::shared_ptr< WindowGL3DAnd2D> w);

protected:
    void onDisplay(Uint32 ms) override;
    void onStep(Uint32 ms) override;

    void onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) override;
    void onKeyChange(SDL_KeyboardEvent &event) override;

    void onMouseMotion(SDL_MouseMotionEvent &event) override;
    void onMouseWheel(SDL_MouseWheelEvent &event) override;
    void onMouseButton(SDL_MouseButtonEvent &event) override;

private:
    Levels levels;
    std::string currentLevelName;
    double t1Magnitude;
    double t2Magnitude;
    Character character;
    bool freeCamera;
    Vector3D cameraOffset;
    Vector3D heightDeviation;
    bool periodicRotation;
    bool timeBendersPauseTime;
    bool pauseSimulation;

    Physics physics;
    OmegachronUi ui;

    std::weak_ptr<WindowGL3DAnd2D> window3DAnd2D;
    WindowGL3DAnd2D &tryGetWindow();

    void reloadLevel(std::string levelName);
    inline Level &getCurrentLevel();
    void resetTime();

    TimeBenders timeBendersAffectingCharacter();
    Vector3D limitSpeed(const Vector3D &newSpeed);

    void tryAdvanceLevel();

    bool isDead();
};


#endif //MYTEMPLATEPROJECT_MANAGER_H
