
#include <randomize/utils/SignalHandler.h>
#include "Manager.h"
#include "graphics/WindowGl3DAnd2D.h"

int main(){
    SigsegvPrinter::activate();

    Dispatcher dispatcher;
    dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();},
            [] (SDL_Event &event){
                return event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE);
            }
    );

    auto window = std::make_shared< WindowGL3DAnd2D>(1280, 960);
    window->open();
    window->initGL();
    dispatcher.addEventHandler( [&dispatcher,&window] (SDL_Event &event){ window->close();},
            [windowID = window->getWindowID()] (SDL_Event &event){
                return event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE;
            }
    );

    Manager manager( dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();

    exit(0);
    return 0;
}

