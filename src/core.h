/**
 * @file core.h
 */

#ifndef OMEGACHRON_CORE_H
#define OMEGACHRON_CORE_H

struct Color {
    float r, g, b, a;
};

const Color GREEN{0.2, 1, 0.2};
const Color ANTI_GREEN{1, 0.2, 1};
const Color ORANGE{1, 0.5, 0.2};
const Color ANTI_ORANGE{0.2, 0.5, 1};
const Color GREEN_ORANGE{0.6, 0.75, 0.2};
const Color ANTI_GREEN_ORANGE{0.6, 0.25, 1};
const Color BLUE{0.4, 0.4, 1.0};
const Color YELLOW{0.8, 0.8, 0.2};
const Color LIME{0.5, 0.8, 0.2};
const Color GREY{0.2, 0.2, 0.2};

extern double tunableValueWithSlider;

#endif //OMEGACHRON_CORE_H
