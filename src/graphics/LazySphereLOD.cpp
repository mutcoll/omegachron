/**
 * @file LazySphereLOD.cpp
 */

#include <GL/gl.h>
#include "LazySphereLOD.h"

std::map<int, LazySphereLOD::Model> LazySphereLOD::lodModels;

void LazySphereLOD::draw(int polygonOrderOfMagnitude) {
    LazySphereLOD::Model &model = getOrGenerateModel(polygonOrderOfMagnitude);
    draw(model);
}

LazySphereLOD::Model& LazySphereLOD::getOrGenerateModel(int polygonOrderOfMagnitude) {
    auto iterator = lodModels.find(polygonOrderOfMagnitude);
    bool modelCached = iterator != lodModels.end();
    if (modelCached) {
        return iterator->second;
    } else {
        return generateModel(polygonOrderOfMagnitude);
    }
}

void LazySphereLOD::draw(const Model &model) {
    glPushMatrix();
    glTranslated(center.x, center.y, center.z);
    glScaled(radius, radius, radius);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_DOUBLE, 0, model.data());
    glDrawArrays(GL_LINES, 0, model.size()*3);
    glDisableClientState(GL_VERTEX_ARRAY);
    glPopMatrix();
}

LazySphereLOD::Model & LazySphereLOD::generateModel(int polygonOrderOfMagnitude) {
    const int MAX_POLYGON_ORDER = 20;
    polygonOrderOfMagnitude = std::min(MAX_POLYGON_ORDER, polygonOrderOfMagnitude);
    auto &model = lodModels[polygonOrderOfMagnitude];

    if (polygonOrderOfMagnitude == 0) {
        //tetrahedron
        auto apothem = sqrt(1 / 3.0);

        // fitting a tetrahedron vertices with a cube vertices
        std::vector<Vector3D> vertices = {
                {+apothem, -apothem, +apothem},
                {+apothem, +apothem, -apothem},
                {-apothem, +apothem, +apothem},
                {-apothem, -apothem, -apothem},
        };

        // 4 take 3. combinating the vertices to form the tetrahedron faces
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[1], vertices[2]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[3], vertices[1]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[2], vertices[3]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[1], vertices[3], vertices[2]}, model);
    } else {
        // octagons vertices match the center of the faces of a cube
        std::vector<Vector3D> vertices = {
                {+1, 0, 0},
                {-1, 0, 0},
                {0, +1, 0},
                {0, -1, 0},
                {0, 0, +1},
                {0, 0, -1},
        };
        // combinating the vertices to form the octagon faces
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[2], vertices[4]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[4], vertices[3]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[3], vertices[5]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[0], vertices[5], vertices[2]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[1], vertices[2], vertices[5]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[1], vertices[5], vertices[3]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[1], vertices[3], vertices[4]}, model);
        generateFace(polygonOrderOfMagnitude, {vertices[1], vertices[4], vertices[2]}, model);
    }
    return model;
}

Vector3D & LazySphereLOD::getPos() {
    return center;
}

double &LazySphereLOD::getRadius() {
    return radius;
}

void LazySphereLOD::generateFace(int detail, std::array<Vector3D, 3> faceVertices, Model &model) {
    if (detail <= 1) {
        model.push_back(faceVertices);
    } else {
        detail--;
        std::vector<Vector3D> middleVertices;
        for (unsigned int i = 0; i < 3; ++i) {
            middleVertices.push_back(middleVertex(faceVertices[i], faceVertices[(i + 1) % 3]));
        }
        generateFace(detail, {faceVertices[0], middleVertices[2], middleVertices[0]}, model);
        generateFace(detail, {faceVertices[1], middleVertices[0], middleVertices[1]}, model);
        generateFace(detail, {faceVertices[2], middleVertices[1], middleVertices[2]}, model);
        generateFace(detail, {middleVertices[0], middleVertices[1], middleVertices[2]}, model);
    }
}

Vector3D LazySphereLOD::middleVertex(const Vector3D& u, const Vector3D& v) {
    return ((u + v) / 2).Unit();
}
