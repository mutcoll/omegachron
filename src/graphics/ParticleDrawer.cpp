/**
 * @file ParticleDrawer.cpp
 */

#include "ParticleDrawer.h"
#include "SphereLOD.h"

void ParticleDrawer::draw(const Particle &particle) {
    SphereLOD sphere{particle.getConstPosition(), particle.getConstRadius(), colorSeed++};
    sphere.draw(3);
}
