/**
 * @file Geometry.h
 */

#ifndef OMEGACHRON_GEOMETRY_H
#define OMEGACHRON_GEOMETRY_H

#include <vector>
#include <entities/Particle.h>
#include "randomize/math/Geom.h"
#include "randomize/math/Vector3D.h"

using Quad = randomize::math::Quad;
using Segment = randomize::math::Segment;

inline Quad buildPlane(Vector3D p0, Vector3D p0p1, Vector3D p0p3) {
    Quad plane;
    plane.p[0] = p0;
    plane.p[1] = p0 + p0p1;
    plane.p[2] = p0 + p0p1 + p0p3;
    plane.p[3] = p0 + p0p3;
    plane.n = (p0p1 ^ p0p3).Unit();
    return plane;
}

inline bool angleDifferenceIsEqualOrLessThan90Degrees(const Vector3D &first, const Vector3D &second) {
    return first * second >= 0;
}

/**
 * Computes if a point is inside the edges of a convex plane.
 *
 * The point has to be coplanar. Otherwise it returns true if the point is inside something similar to a four-sided
 * sphere around the plane.
 */
inline bool insidePlane (const Quad &plane, const Vector3D &point) {
    Vector3D pointToP0(plane.p[0] - point);
    Vector3D pointToP1(plane.p[1] - point);
    Vector3D pointToP2(plane.p[2] - point);
    Vector3D pointToP3(plane.p[3] - point);

    Vector3D normal1(pointToP0 ^ pointToP1);
    Vector3D normal2(pointToP1 ^ pointToP2);
    Vector3D normal3(pointToP2 ^ pointToP3);
    Vector3D normal4(pointToP3 ^ pointToP0);

    bool normals1And2PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal1, normal2);
    bool normals1And3PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal1, normal3);
    bool normals1And4PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal1, normal4);
    bool normals2And3PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal2, normal3);
    bool normals2And4PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal2, normal4);
    bool normals3And4PointToTheSameSide = angleDifferenceIsEqualOrLessThan90Degrees(normal3, normal4);

    bool inside = normals1And2PointToTheSameSide
            and normals1And3PointToTheSameSide
            and normals1And4PointToTheSameSide
            and normals2And3PointToTheSameSide
            and normals2And4PointToTheSameSide
            and normals3And4PointToTheSameSide;

    return inside;
}

/**
 * Computes if the perpendicular projection of a point in a plane falls inside or outside.
 *
 * Returns by reference the minimum perpendicular distance (without considering plane limits) from the plane to the
 * point (positive if the point is in the same direction as the plane normal)
 * and the projected point, which is coplanar to the plane vertices but can be inside or outside the plane limits.
 *
 * @return as if it was a bool. 1 means inside, 0 means outside.
 */
inline bool fallsInsideConvexPlane(const Quad &plane, const Vector3D &point, double &distance, Vector3D &projection) {
    distance = (point - plane.p[0]) * plane.n;    // projection of the point on the plane normal
    projection = point - distance * plane.n;
    return insidePlane(plane, projection);
}

inline bool spheresIntersect(const Vector3D &firstPosition, double firstRadius,
        const Vector3D &secondPosition, double secondRadius) {
    bool intersect = (firstPosition - secondPosition).Modulo() < firstRadius + secondRadius;
    return intersect;
}

inline bool cubesIntersect(const Vector3D &firstPosition, double firstHalfSize,
        const Vector3D &secondPosition, double secondHalfSize) {
    auto difference = secondPosition - firstPosition;
    bool intersects = std::abs(difference.x) - secondHalfSize < firstHalfSize
            and std::abs(difference.y) - secondHalfSize < firstHalfSize
            and std::abs(difference.z) - secondHalfSize < firstHalfSize;
    return intersects;
}

/**
 * @param distanceFromPointInTheLineToProjection will be negative if the outerPoint is in the opposite direction than
 * lineDirection
 * @return perpendicular projection of the outer point on the line
 */
inline Vector3D getPointProjectionOverLine(const Vector3D &outerPoint, const Vector3D &pointInTheLine,
        const Vector3D &lineDirection, double &distanceFromPointInTheLineToProjection) {
    auto pointInTheLineToOuterPoint = outerPoint - pointInTheLine;
    distanceFromPointInTheLineToProjection = lineDirection * pointInTheLineToOuterPoint;
    auto p0ToProjection = lineDirection * distanceFromPointInTheLineToProjection;
    auto projection = pointInTheLine + p0ToProjection;
    return projection;
}

inline std::vector<Vector3D> intersectionsBetweenSphereAndSegment(const Vector3D &spherePosition, double radius,
        const Segment &segment) {
    auto p0ToP1 = segment.P1 - segment.P0;
    auto distanceFromP0ToP1 = p0ToP1.Modulo();
    auto segmentDirection = p0ToP1 / distanceFromP0ToP1;
    auto p0ToSphere = spherePosition - segment.P0;
    auto normal = p0ToSphere ^ segmentDirection;

    // given that vector product is "magnitudeOf(a ^ b) == magnitudeOf(a) * magnitudeOf(b) * sin(angleBetween(a, b))"
    // and "magnitudeOf(segmentDirection) == 1"
    // and "sin(angleBetween(p0ToSphere, segmentDirection)) == distanceFromSphereToSegment / magnitudeOf(p0ToSphere)"
    auto distanceFromSphereToSegment = normal.Modulo();

    bool intersectsInfiniteLine = distanceFromSphereToSegment < radius;
    
    std::vector<Vector3D> intersections;
    if (intersectsInfiniteLine) {
        double distanceFromP0ToProjection;
        auto projection = getPointProjectionOverLine(spherePosition, segment.P0, segmentDirection,
                distanceFromP0ToProjection);
        auto distanceFromProjectionToIntersection = sqrt(
                radius * radius - distanceFromSphereToSegment * distanceFromSphereToSegment);
        
        bool p0OutsideSphere = p0ToSphere.Modulo() > radius;
        bool p1OutsideSphere = (spherePosition - segment.P1).Modulo() > radius;
        if (p0OutsideSphere and p1OutsideSphere) {
            if (distanceFromP0ToProjection >= 0 and distanceFromP0ToProjection <= distanceFromP0ToP1) {
                intersections.push_back(projection + segmentDirection * distanceFromProjectionToIntersection);
                intersections.push_back(projection - segmentDirection * distanceFromProjectionToIntersection);
            } else {
                // intersection with the line but not with the segment: intersects further away than the segment ends
            }
        } else if (p0OutsideSphere) {
            intersections.push_back(projection - segmentDirection * distanceFromProjectionToIntersection);
        } else if (p1OutsideSphere) {
            intersections.push_back(projection + segmentDirection * distanceFromProjectionToIntersection);
        }
    }
    return intersections;
}

inline std::vector<Vector3D> collectEdgeIntersections(const Vector3D &spherePosition,
        const double &sphereRadius, const Quad &plane) {
    std::vector<Vector3D> intersections;
    auto partialIntersection = intersectionsBetweenSphereAndSegment(spherePosition,
            sphereRadius, Segment{plane.p[0], plane.p[1]});
    intersections.insert(intersections.end(), partialIntersection.begin(), partialIntersection.end());
    partialIntersection = intersectionsBetweenSphereAndSegment(spherePosition,
            sphereRadius, Segment{plane.p[1], plane.p[2]});
    intersections.insert(intersections.end(), partialIntersection.begin(), partialIntersection.end());
    partialIntersection = intersectionsBetweenSphereAndSegment(spherePosition,
            sphereRadius, Segment{plane.p[2], plane.p[3]});
    intersections.insert(intersections.end(), partialIntersection.begin(), partialIntersection.end());
    partialIntersection = intersectionsBetweenSphereAndSegment(spherePosition,
            sphereRadius, Segment{plane.p[3], plane.p[0]});
    intersections.insert(intersections.end(), partialIntersection.begin(), partialIntersection.end());
    return intersections;
}

#endif //OMEGACHRON_GEOMETRY_H
