/**
 * @file LazySphereLOD.h
 */

#ifndef GRAVITYTAMER_LAZYSPHERELOD_H
#define GRAVITYTAMER_LAZYSPHERELOD_H

#include <algorithm>
#include <array>
#include <map>
#include <random>
#include "randomize/math/Vector3D.h"

class LazySphereLOD {
public:

    LazySphereLOD(const Vector3D &center, double radius)
            : center(center), radius(radius), distribution(0.5, 1.0) {
    }
    LazySphereLOD(const Vector3D &center, double radius, long colorSeed)
            : center(center), radius(radius), generator(colorSeed), distribution(0.5, 1.0),
            colorSeed(colorSeed) {
    }

    void draw(int polygonOrderOfMagnitude);
    Vector3D & getPos();
    double & getRadius();
    int getFacesDrawn();

private:
    Vector3D center;
    double radius;
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution;
    // distance in a cube from center to an edge if the distance to a vertex is 1
    float apothem;
    long colorSeed;

    void drawFace(int detail, std::array<Vector3D, 3> faceVertices);

    static Vector3D middleVertex(const Vector3D& u, const Vector3D& v);

    float hash(int seed) const;

    using Triangle = std::array<Vector3D, 3>;
    using Model = std::vector<Triangle>;
    static std::map<int, Model> lodModels;

    static Model & generateModel(int polygonOrderOfMagnitude);

    void draw(const Model &model);

    static void generateFace(int detail, std::array<Vector3D, 3> faceVertices, Model &model);

    Model& getOrGenerateModel(int polygonOrderOfMagnitude);
};


#endif //GRAVITYTAMER_LAZYSPHERELOD_H
