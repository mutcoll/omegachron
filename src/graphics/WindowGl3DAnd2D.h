/**
 * @file WindowGl3DAnd2D.h
 */

#ifndef OMEGACHRON_WINDOWGL3DAND2D_H
#define OMEGACHRON_WINDOWGL3DAND2D_H


#include <randomize/graphics/Window.h>
#include <iostream>

class WindowGL3DAnd2D : public WindowGL {
public:
    using WindowGL::WindowGL;

    void setWindowTitle(std::string title) {
        SDL_SetWindowTitle(window, title.c_str());
    }

    using WindowGL::initGL;
    void initGL(int width, int height) override {
        WindowGL::initGL(width, height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();                // Reset The Projection Matrix
        auto aspect = (GLdouble) width / height;
        this->perspectiveGL (45, aspect, 0.1, 500);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glMatrixMode(GL_MODELVIEW);
    }

    GLdouble getAspectRatio() {
        int width, height;
        getWindowSize(width, height);
        auto aspect = (GLdouble) width / height;
        return aspect;
    }
};


#endif //OMEGACHRON_WINDOWGL3DAND2D_H
