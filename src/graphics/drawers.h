/**
 * @file drawers.h
 */

#ifndef OMEGACHRON_DRAWERS_H
#define OMEGACHRON_DRAWERS_H

#include <vector>
#include <randomize/graphics/Window.h>
#include "physics/Levels.h"
//#include "math/Geom.h"
#include "core.h"
#include "entities/TimeBender.h"
#include "entities/Character.h"
#include "entities/Particle.h"
#include "graphics/Geometry.h"

void drawLevel(const Level &level);
void drawPlatform(const Platform &platform);
void drawPlane(const Quad &plane);
void drawParticle(const BiTimedParticle &particle);
void drawStick(const Vector3D &start, const Vector3D &end, double thickness, const Color &c);
void drawCharacter(const Character &character);
void drawTimeBender(const TimeBender &timeBender);
void drawCube(float size, Color color);
void drawCube(Vector3D position, float size, Color color);
void drawFence(Vector3D position, float size, Color color);
void drawAxes();


#endif //OMEGACHRON_DRAWERS_H
