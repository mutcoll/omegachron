/**
 * @file ParticleDrawer.h
 */

#ifndef PARTICLEAUTOMATON_PARTICLEDRAWER_H
#define PARTICLEAUTOMATON_PARTICLEDRAWER_H

#include "entities/Particle.h"

class ParticleDrawer {
public:
    void draw(const Particle &particle);
    int colorSeed = 1;
};


#endif //PARTICLEAUTOMATON_PARTICLEDRAWER_H
