/**
 * @file drawers.cpp
 */

#include "drawers.h"
#include "SphereLOD.h"
#include "LazySphereLOD.h"


std::pair<Vector3D, Vector3D> getAxisUpAndRightSide(const Vector3D &direction, double magnitude);

void drawGoal(const Level::Goal &goal);

void drawLevel(const Level &level) {
    for (const auto &plane : level.planes) {
        drawPlane(plane);
    }
    for (const auto &timeBender : level.timeBenders) {
        drawTimeBender(timeBender);
    }
    for (const auto &particle : level.particles) {
        drawParticle(particle);
    }
    for (const auto &platform : level.platforms) {
        drawPlatform(platform);
    }
    drawGoal(level.goal);
}

void drawGoal(const Level::Goal &goal) {
    drawCube(goal.center + Vector3D{0, goal.size/2, 0}, goal.size, BLUE);
}

void drawPlatform(const Platform &platform) {
    glPushMatrix();
    glTranslated(platform.center.x, platform.center.y, platform.center.z);
    glScaled(platform.sizeX, platform.sizeY, platform.sizeZ);
    drawCube(1, platform.timeRotation == TimeRotation::TOWARDS_T1 ? GREEN : ORANGE);
    glPopMatrix();

//    glBegin(GL_LINES);
//    auto resolution = 2.0;
//    auto sizeCoefficient = 0.2;
//    auto planes = platform.getPlanes();
//    double offset = 0;
//    for (const auto &plane : planes) {
//        offset += 0.05;
//        glColor3f(0.3, offset*3, 0.7);
//        for (size_t ix = 0; ix < platform.sizeX * resolution * sizeCoefficient; ++ix) {
//            for (size_t iy = 0; iy < platform.sizeY * resolution * sizeCoefficient; ++iy) {
//                for (size_t iz = 0; iz < platform.sizeZ * resolution * sizeCoefficient*3; ++iz) {
//                    auto point = Vector3D{
//                            platform.center.x + platform.sizeX *0.5*(1-sizeCoefficient) + ix / resolution + offset,
//                            platform.center.y + platform.sizeY *0.5*(1-sizeCoefficient) + iy / resolution,
//                            platform.center.z + platform.sizeZ     *(1-sizeCoefficient) + iz / resolution};
//                    double distance;
//                    Vector3D projection;
//                    if (not fallsInsideConvexPlane(plane, point, distance, projection)) {
//                        auto intersections = collectEdgeIntersections(point, 1, plane);
//                        for (const auto &intersection : intersections) {
//                            glVertex3dv(point.v);
//                            glVertex3dv(intersection.v);
//                        }
//                    } else {
//                        glVertex3dv(point.v);
//                        glVertex3dv((point + Vector3D{0, 0.01, 0.05}).v);
//                    }
//                }
//            }
//        }
//    }
//    glEnd();
}

void drawPlane(const Quad &plane) {
//    glBegin(GL_TRIANGLE_FAN);
//    glColor3f(GREY.r, GREY.g, GREY.b);
//    glVertex3dv(plane.p[0].v);
//    glVertex3dv(plane.p[1].v);
//    glVertex3dv(plane.p[2].v);
//    glVertex3dv(plane.p[3].v);
//    glEnd();
    glBegin(GL_LINES);
    glColor3f(0.8, 0.8, 0.8);
    auto p0 = plane.p[0] + plane.n.Unit() * 0.01;
    auto p0p1 = plane.p[1] - plane.p[0];
    auto p0p1Unit = p0p1.Unit();
    auto p0p3 = plane.p[3] - plane.p[0];
    auto p0p3Unit = p0p3.Unit();
    auto p0p3Index = p0p3Unit;
    auto p0p1Index = p0p1Unit;
    auto p0p1Modulus = p0p1.Modulo();
    auto p0p3Modulus = p0p3.Modulo();
    for (int i = 0; i <= p0p1Modulus; ++i) {
        glVertex3dv((p0 + p0p1Index * i).v);
        glVertex3dv((p0 + p0p3 + p0p1Index * i).v);
    }
    for (int i = 0; i <= p0p3Modulus; ++i) {
        glVertex3dv((p0 + p0p3Index * i).v);
        glVertex3dv((p0 + p0p1 + p0p3Index * i).v);
    }
    glEnd();
}


void drawParticle(const BiTimedParticle &particle) {
    static int colorSeed = 1;
    LazySphereLOD sphere{particle.getConstPosition(), particle.getConstRadius(), colorSeed};//++ % 2+1};
    glColor3f(BLUE.r, BLUE.g, BLUE.b);
    sphere.draw(2);
    if (particle.getSpeed1().Modulo() != 0) {
        drawStick(particle.getPosition(),
                particle.getPosition() + particle.getSpeed1().Unit() * particle.getRadius() * 2,
                particle.getRadius() * 0.5, GREEN);
    }
    if (particle.getSpeed2().Modulo() != 0) {
        drawStick(particle.getPosition(),
                particle.getPosition() + particle.getSpeed2().Unit() * particle.getRadius() * 2,
                particle.getRadius() * 0.5, ORANGE);
    }
}

std::pair<Vector3D, Vector3D> getAxisUpAndRightSide(const Vector3D &direction, double magnitude) {
    auto up = Vector3D{0, 1, 0};

    if ((direction ^ up) == Vector3D{0, 0, 0}) {
        up = Vector3D{0, 0, 1};
    }
    auto rightSide = direction ^ up;
    up = direction ^ rightSide;

    up = up.Unit() * magnitude;
    rightSide = rightSide.Unit() * magnitude;
    return {up, rightSide};
}


void drawStick(const Vector3D &start, const Vector3D &end, double thickness, const Color &c) {
    auto direction = end - start;
    Vector3D up, rightSide;
    std::tie(up, rightSide) = getAxisUpAndRightSide(direction, thickness);


    glBegin(GL_TRIANGLE_STRIP);

    glColor3f(c.r, c.g, c.b);
    glVertex3dv((start + up + rightSide).v);
    glVertex3dv((end + up + rightSide).v);

    glVertex3dv((start - up + rightSide).v);
    glVertex3dv((end - up + rightSide).v);

    glVertex3dv((start - up - rightSide).v);
    glVertex3dv((end - up - rightSide).v);

    glVertex3dv((start + up - rightSide).v);
    glVertex3dv((end + up - rightSide).v);

    glVertex3dv((start + up + rightSide).v);
    glVertex3dv((end + up + rightSide).v);

    glEnd();
}

void drawCharacter(const Character &character) {
    glPushMatrix();
    auto height = 2;
    glTranslated(character.position.x, character.position.y, character.position.z);
    glTranslatef(0, character.size / 2.0 * (height - 1), 0);
    glScalef(1, height, 1);
    drawCube(character.size, BLUE);
    glPopMatrix();
}

void drawTimeBender(const TimeBender &timeBender) {
    drawCube(timeBender.centerPosition, timeBender.size, timeBender.color);
    auto fencePosition = timeBender.centerPosition;
    fencePosition[1] -= timeBender.size / 2.0;
    drawFence(fencePosition, timeBender.size * 8, timeBender.color);
}

void drawCube(float size, Color color) {
    drawCube({0, 0, 0}, size, color);
}

void drawCube(Vector3D position, float size, Color color) {
    glPushMatrix();
        glTranslatef(position[0] - size/2.0, position[1] - size/2.0, position[2] - size/2.0);
        glBegin(GL_TRIANGLE_FAN);
            glColor3f(color.r, color.g, color.b);
            glVertex3f(0, 0, 0);
            glVertex3f(size, 0, 0);
            glVertex3f(size, size, 0);
            glVertex3f(0, size, 0);
            glVertex3f(0, size, size);
            glVertex3f(0, 0, size);
            glVertex3f(size, 0, size);
            glVertex3f(size, 0, 0);
        glEnd();
        glBegin(GL_TRIANGLE_FAN);
            glColor3f(color.r, color.g, color.b);
            glVertex3f(size, size, size);
            glVertex3f(0, size, size);
            glVertex3f(0, 0, size);
            glVertex3f(size, 0, size);
            glVertex3f(size, 0, 0);
            glVertex3f(size, size, 0);
            glVertex3f(0, size, 0);
            glVertex3f(0, size, size);
        glEnd();
    glPopMatrix();
}

void drawFence(Vector3D position, float size, Color color) {
    glPushMatrix();
    glTranslatef(position[0] - size/2.0, position[1], position[2] - size/2.0);
    glBegin(GL_TRIANGLE_STRIP);
    glColor3f(color.r, color.g, color.b);
    auto height = 0.5;
    glVertex3f(0, 0, 0);
    glVertex3f(0, height, 0);
    glVertex3f(size, 0, 0);
    glVertex3f(size, height, 0);
    glVertex3f(size, 0, size);
    glVertex3f(size, height, size);
    glVertex3f(0, 0, size);
    glVertex3f(0, height, size);
    glVertex3f(0, 0, 0);
    glVertex3f(0, height, 0);
    glEnd();
    glPopMatrix();
}

void drawAxes() {
    auto length = 100;
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(length, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, length, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, length);
    glEnd();
    glPointSize(4);
    glBegin(GL_POINTS);
    glColor3f(1, 1, 1);
    for (int i = 1; i < length; ++i) {
        glVertex3f(i, 0, 0);
        glVertex3f(0, i, 0);
        glVertex3f(0, 0, i);
    }
    glEnd();
}

