/**
 * @file SphereLOD.cpp
 */

#include <GL/gl.h>
#include "SphereLOD.h"

void SphereLOD::draw(double polygonOrderOfMagnitude) {
    const double MAX_POLYGON_ORDER = 20;
    polygonOrderOfMagnitude = std::min(MAX_POLYGON_ORDER, polygonOrderOfMagnitude);

    facesDrawn = 0;
    if (polygonOrderOfMagnitude == 0) {
        //tetrahedron
        apothem = std::sqrt(radius * radius / 3.0);

        // fitting a tetrahedron vertices with a cube vertices
        std::vector<Vector3D> vertices = {
                {center.x + apothem, center.y - apothem, center.z + apothem},
                {center.x + apothem, center.y + apothem, center.z - apothem},
                {center.x - apothem, center.y + apothem, center.z + apothem},
                {center.x - apothem, center.y - apothem, center.z - apothem},
        };

        glBegin(GL_TRIANGLES);
        // 4 take 3. combinating the vertices to form the tetrahedron faces
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[1], vertices[2]});
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[3], vertices[1]});
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[2], vertices[3]});
        drawFace(polygonOrderOfMagnitude, {vertices[1], vertices[3], vertices[2]});
        glEnd();
    } else {
        // octagons vertices match the center of the faces of a cube
        std::vector<Vector3D> vertices = {
                {center.x + radius, center.y, center.z},
                {center.x - radius, center.y, center.z},
                {center.x, center.y + radius, center.z},
                {center.x, center.y - radius, center.z},
                {center.x, center.y, center.z + radius},
                {center.x, center.y, center.z - radius},
        };
        glBegin(GL_TRIANGLES);
        // combinating the vertices to form the octagon faces
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[2], vertices[4]});
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[4], vertices[3]});
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[3], vertices[5]});
        drawFace(polygonOrderOfMagnitude, {vertices[0], vertices[5], vertices[2]});
        drawFace(polygonOrderOfMagnitude, {vertices[1], vertices[2], vertices[5]});
        drawFace(polygonOrderOfMagnitude, {vertices[1], vertices[5], vertices[3]});
        drawFace(polygonOrderOfMagnitude, {vertices[1], vertices[3], vertices[4]});
        drawFace(polygonOrderOfMagnitude, {vertices[1], vertices[4], vertices[2]});
        glEnd();
    }
}

Vector3D & SphereLOD::getPos() {
    return center;
}

double &SphereLOD::getRadius() {
    return radius;
}

/**
 * requires an environment of glBegin(GL_TRIANGLES) to be set
 */
void SphereLOD::drawFace(int detail, std::array<Vector3D, 3> faceVertices) {
    if (detail <= 1) {
        facesDrawn++;
        for (auto vertex : faceVertices) {
            glColor3f(hash(32), hash(64), hash(128));
//            glColor3f(0.5, 0.5, 0.5);
//            glColor3f(distribution(generator), distribution(generator), distribution(generator));
            glVertex3dv(vertex.v);
        }
    } else {
        detail--;
        std::vector<Vector3D> middleVertices;
        for (unsigned int i = 0; i < 3; ++i) {
            middleVertices.push_back(middleVertex(faceVertices[i], faceVertices[(i +1) %3]));
        }
        drawFace(detail, {faceVertices[0], middleVertices[2], middleVertices[0]});
        drawFace(detail, {faceVertices[1], middleVertices[0], middleVertices[1]});
        drawFace(detail, {faceVertices[2], middleVertices[1], middleVertices[2]});
        drawFace(detail, {middleVertices[0], middleVertices[1], middleVertices[2]});
    }
}

float SphereLOD::hash(int seed) const {
    return (colorSeed * facesDrawn % seed) / float(seed);
}


Vector3D SphereLOD::middleVertex(Vector3D u, Vector3D v) {
    return ((u + v)/2 - center).Unit() * radius + center;
}

int SphereLOD::getFacesDrawn() {
    return facesDrawn;
}
