# OmegaChron

Experimental game to play with 2 time dimensions.

## Compilation requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), or instead:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)

## How to compile

Linux:

```
git clone https://bitbucket.org/mutcoll/omegachron.git
cd omegachron
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
```

That will create an executable binary as `bin/omegachron`.

## How to run

To load the resources properly, run the game from the project root folder:
```
./build/bin/omegachron
```
